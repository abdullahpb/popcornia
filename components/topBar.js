import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    backgroundColor: "#FFF",
    height: "100px",
    backgroundColor: "#FFF",
    borderRight: "0px"
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    },
    backgroundColor: "#FFF0",
    borderRight: "0px"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

export default function MiniDrawer() {
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={clsx(classes.appBar)}>
      <Toolbar>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <img src="/static/popcorn2.png" width="150px" height="100px" />
          </Grid>

          <Grid item xs={6} style={{ textAlign: "center" }}>
            <img src="/static/popcorn1.png" width="150px" height="100px" />
          </Grid>
          <Grid item xs={3} />
        </Grid>
      </Toolbar>
    </AppBar>
  );
}
