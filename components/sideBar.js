import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Home from "@material-ui/icons/Home";
import Dashboard from "@material-ui/icons/Dashboard";
import { Link } from "@material-ui/core";

const drawerWidth = 240;

const header = [
  { key: 1, label: <Home />, value: "/index", name: "Home" },
  { key: 2, label: <Dashboard />, value: "/dashboard", name: "Dashboard" },
  { key: 3, label: <Home />, value: "/setup", name: "Setup" },
  {
    key: 4,
    label: <Dashboard />,
    value: "/procurement",
    name: "Procurement"
  },
  { key: 5, label: <Home />, value: "/production", name: "Production" },
  { key: 6, label: <Dashboard />, value: "/sales", name: "Sales" },
  { key: 7, label: <Home />, value: "/accounting", name: "Accounting" },
  { key: 8, label: <Dashboard />, value: "/reports", name: "Reports" }
];

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  menuButton: {
    marginRight: 36
  },
  link: {
    textDecoration: "none"
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    }),
    backgroundColor: "#FFF0",
    borderRight: "0px",
    marginRight: "100px"
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    },
    backgroundColor: "#FFF0",
    borderRight: "0px"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

export default function MiniDrawer() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
        open={open}
        onMouseLeave={handleDrawerClose}
      >
        <div className={classes.toolbar} />
        <List onMouseEnter={handleDrawerOpen} style={{ paddingTop: "50px" }}>
          {header.map(text => (
            <Link
              key={text.key}
              variant="button"
              color="textPrimary"
              href={text.value}
              className={classes.link}
              underline={"none"}
            >
              <ListItem button>
                <ListItemIcon>{text.label}</ListItemIcon>
                <ListItemText primary={text.name} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
    </div>
  );
}
