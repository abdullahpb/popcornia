import React, { Component } from "react";
import { DatePicker, KeyboardDatePicker } from "@material-ui/pickers";
import { Typography } from "@material-ui/core";

class DatePickerMMDDYYY extends Component {
  state = {
    value:
      this.props.initialValue == null ? null : new Date(this.props.initialValue)
  };

  render() {
    return (
      <React.Fragment>
        <KeyboardDatePicker
          label={this.props.label}
          inputVariant="outlined"
          error={
            this.props.formik.errors[this.props.name] &&
            this.props.formik.touched[this.props.name]
              ? true
              : false
          }
          margin="normal"
          value={this.state.value}
          style={{ width: "95%" }}
          onChange={date => {
            this.setState({ value: date });
            this.props.formik.values[this.props.name] = date;
          }}
          helperText={
            this.props.formik.errors[this.props.name] &&
            this.props.formik.touched[this.props.name]
              ? this.props.formik.errors[this.props.name]
              : ""
          }
          format="MM/dd/yyyy"
          InputLabelProps={{
            shrink: true
          }}
        />
        {/* {this.props.formik.errors[this.props.name] ? (
          <Typography
            style={{ color: "#FF0000", margin: 0, padding: 0 }}
            variant="caption"
          >
            Required
          </Typography>
        ) : (
          ""
        )} */}
      </React.Fragment>
    );
  }
}

export default DatePickerMMDDYYY;
