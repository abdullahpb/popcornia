import React, { Component } from "react";
import {
  TextField,
  Grid,
  MenuItem,
  Radio,
  RadioGroup,
  FormControlLabel,
  Typography,
  Button
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import * as FormikUi from "formik-material-ui";
import DatePickerMM from "./datepicker-mmddyyy";
import AutoComplete from "./autocomplete.js";
import SupplierJson from "../assets/json/supplierJson.json";
import { connect } from "react-redux";
import * as actionCreator from "../redux/importBill/action";
import * as apiCallCreator from "../redux/importBill/axios";
const ItemJson = [
  {
    label: "Gold Corn",
    value: "GC"
  },
  {
    label: "Silver Corn",
    value: "SC"
  }
];

class ImportSearch extends Component {
  state = {
    searchType: "byAll",
    searchDateNumber: "NUMBER"
  };

  render() {
    return (
      <Grid container>
        <Grid item xs={3}>
          <TextField
            id="search-selector"
            select
            label="Search By"
            value={this.state.searchType}
            onChange={event => {
              this.setState({ searchType: event.target.value });
              if (event.target.value) {
                apiCallCreator.getAllInvoice(this.props.getAllInvoice);
              }
            }}
            style={{ width: 120 }}
            margin="normal"
          >
            <MenuItem value="byAll">By All</MenuItem>
            <MenuItem value="transaction">Transaction</MenuItem>
            <MenuItem value="invoice">Invoice</MenuItem>
            <MenuItem value="supplier">Supplier</MenuItem>
            <MenuItem value="item">Item</MenuItem>
          </TextField>
        </Grid>
        {this.state.searchType == "transaction" ||
        this.state.searchType == "invoice" ? (
          <Grid item xs={9}>
            <Grid container alignItems="center">
              <Grid item xs={12}>
                <RadioGroup
                  row
                  aria-label="gender"
                  name="gender2"
                  value={this.state.searchDateNumber}
                  onChange={val =>
                    this.setState({
                      searchDateNumber: val.target.value
                    })
                  }
                >
                  <FormControlLabel
                    value="DATE"
                    control={<Radio color="primary" />}
                    label={
                      <Typography variant="caption" display="block">
                        By Date
                      </Typography>
                    }
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    value="NUMBER"
                    control={<Radio color="primary" />}
                    label={
                      <Typography variant="caption" display="block">
                        By Number
                      </Typography>
                    }
                    labelPlacement="end"
                  />
                </RadioGroup>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={12}>
                <Formik
                  initialValues={{
                    from: "",
                    to: "",
                    val: ""
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                    let doc = {};
                    if (this.state.searchType == "transaction") {
                      if (this.state.searchDateNumber == "DATE") {
                        doc = {
                          type: "transactionDate",
                          from: values.from,
                          to: values.to
                        };
                      }
                      if (this.state.searchDateNumber == "NUMBER") {
                        doc = {
                          transactionNo: values.val
                        };
                      }
                    }
                    if (this.state.searchType == "invoice") {
                      if (this.state.searchDateNumber == "DATE") {
                        doc = {
                          type: "invoiceDate",
                          from: values.from,
                          to: values.to
                        };
                      }
                      if (this.state.searchDateNumber == "NUMBER") {
                        doc = {
                          invoiceNo: values.val
                        };
                      }
                      setSubmitting();
                    }
                    console.log(doc);
                    apiCallCreator.searchInvoice(doc, this.props.getAllInvoice);
                    // if (this.state.searchDateNumber == "DATE") {
                    //   doc = {
                    //     type: "transactionDate",
                    //     from: values.from,
                    //     to: values.to
                    //   };
                    // }
                    // if (this.state.searchDateNumber == "NUMBER") {
                    //   doc = {
                    //     type: "transactionNo",
                    //     from: values.from,
                    //     to: values.to
                    //   };
                    // }
                    // apiCallCreator.searchInvoice(doc, this.props.getAllInvoice);
                    setSubmitting(false);
                  }}
                >
                  {({ values }) => (
                    <Form autoComplete="off">
                      {this.state.searchDateNumber == "DATE" ? (
                        <Grid container alignItems="center">
                          {fieldMapper(searchDateField, values)}
                          <Grid item xs={4}>
                            <Button
                              type="submit"
                              variant="outlined"
                              color="primary"
                              size="small"
                              style={{
                                margin: 0,
                                width: "100px",
                                borderRadius: "25px"
                              }}
                            >
                              Search
                            </Button>
                          </Grid>
                        </Grid>
                      ) : (
                        ""
                      )}
                      {this.state.searchDateNumber == "NUMBER" ? (
                        <Grid container alignItems="center">
                          {fieldMapper(searchNumberField, values)}
                          <Grid item xs={4}>
                            <Button
                              type="submit"
                              variant="outlined"
                              color="primary"
                              size="small"
                              onClick={() => console.log("eer")}
                              style={{
                                margin: 0,
                                width: "100px",
                                borderRadius: "25px"
                              }}
                            >
                              Search
                            </Button>
                          </Grid>
                        </Grid>
                      ) : (
                        ""
                      )}
                    </Form>
                  )}
                </Formik>
              </Grid>
            </Grid>
          </Grid>
        ) : (
          ""
        )}
        {this.state.searchType == "supplier" ||
        this.state.searchType == "item" ? (
          <Grid item xs={9} style={{ padding: 5 }}>
            <div style={{ width: "95%", marginTop: "16px" }}>
              <Formik
                initialValues={{
                  supplier: "",
                  item: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  // console.log(values);
                  let doc = {
                    supplier: values.supplier
                  };
                  apiCallCreator.searchInvoice(doc, this.props.getAllInvoice);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container alignItems="center">
                      <Grid item xs={8}>
                        {this.state.searchType == "supplier" ? (
                          <Field
                            name=""
                            render={props => {
                              return (
                                <AutoComplete
                                  name="supplier"
                                  formik={props}
                                  suggestions={SupplierJson}
                                  label="Supplier"
                                  value={values.supplier}
                                />
                              );
                            }}
                          />
                        ) : (
                          ""
                        )}
                        {this.state.searchType == "item" ? (
                          <Field
                            name=""
                            render={props => {
                              return (
                                <AutoComplete
                                  name="item"
                                  formik={props}
                                  suggestions={ItemJson}
                                  label="Item"
                                  value={values.supplier}
                                />
                              );
                            }}
                          />
                        ) : (
                          ""
                        )}
                      </Grid>
                      <Grid item xs={4}>
                        <Button
                          type="submit"
                          variant="outlined"
                          color="primary"
                          size="small"
                          style={{
                            margin: 0,
                            marginLeft: 10,
                            width: "100px",
                            borderRadius: "25px"
                          }}
                        >
                          Search
                        </Button>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          </Grid>
        ) : (
          ""
        )}
      </Grid>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getAllInvoice: val => dispatch(actionCreator.GetAllInvoice(val))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ImportSearch);

function fieldMapper(fieldData, values) {
  return fieldData.map((e, i) => {
    switch (e.type) {
      case "autoComplete":
        return (
          <Grid key={i} item xs={e.size}>
            <div style={{ width: "95%", marginTop: "16px" }}>
              <Field
                name={e.name}
                render={props => {
                  return (
                    <AutoComplete
                      key={i + e.name}
                      name={e.name}
                      formik={props}
                      suggestions={e.suggestions}
                      label={e.label}
                      value={values[e.name]}
                      // initialValue={values[e.name]}
                    />
                  );
                }}
              />
            </div>
          </Grid>
        );
      case "select":
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              select
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={FormikUi.TextField}
              margin="normal"
              InputLabelProps={{
                shrink: true
              }}
            >
              <MenuItem value="usd">$</MenuItem>
              <MenuItem value="eur">€</MenuItem>
              <MenuItem value="btc">฿</MenuItem>
              <MenuItem value="jpy">¥</MenuItem>
            </Field>
          </Grid>
        );
      case "date":
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              inputVariant="outlined"
              render={props => {
                return (
                  <DatePickerMM
                    initialValue={values[e.name] == "" ? null : values[e.name]}
                    name={e.name}
                    label={e.label}
                    formik={props.form}
                  />
                );
              }}
            />
          </Grid>
        );
      default:
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              type={e.type}
              value={values[e.name]}
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={FormikUi.TextField}
              margin="normal"
              multiline={e.multiline}
              InputLabelProps={{
                shrink: true
              }}
            />
          </Grid>
        );
    }
  });
}

const searchDateField = [
  { name: "from", label: "From", size: 4, type: "date" },
  { name: "to", label: "To", size: 4, type: "date" }
];

const searchNumberField = [{ name: "val", label: "Number", size: 8 }];
