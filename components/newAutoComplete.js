import Autosuggest from "react-autosuggest";
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";

const styles = {
  container: {
    position: "relative",
    width: "100%"
  },
  // inputOpen: {
  //     borderBottomLeftRadius: 0,
  //     borderBottomRightRadius: 0,
  // },
  // inputFocused: {
  //     outline: "none",
  // },
  suggestionsContainer: {
    display: "none"
  },
  suggestionsContainerOpen: {
    display: "block",
    position: "absolute",
    top: "51px",
    width: "100%",
    border: "1px solid #aaa",
    backgroundColor: "#fff",
    fontFamily: "Helvetica, sans-serif",
    fontWeight: 300,
    fontSize: "16px",
    borderBottomLeftRadius: "4px",
    borderBottomRightRadius: "4px",
    zIndex: 100
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: "none"
  },
  suggestion: {
    cursor: "pointer",
    padding: "10px 20px"
  },
  suggestionHighlighted: {
    backgroundColor: "#ddd"
  }
};

class ReactAutosuggest extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.validateFunction = this.validateFunction.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    );
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(
      this
    );
    this.handleBlur = this.handleBlur.bind(this);
    this.getSuggestions = this.getSuggestions.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.state = {
      value: props.formik.field.value,
      suggestions: [],
      data: this.props.data || [],
      validating: false
    };
  }

  getSuggestionValue = suggestion => suggestion.label;
  renderSuggestion = suggestion => <div>{suggestion.label}</div>;

  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return this.state.data.filter(
      lang => lang.label.toLowerCase().slice(0, inputLength) === inputValue
    );
  };

  handleChange = (event, { newValue }) => {
    if (this.state.suggestions.length == 0) {
      this.setState({ value: "" });
      this.props.formik.form.values[this.props.name] = newValue;
    }
    this.props.formik.form.values[this.props.name] = newValue;
    this.setState({
      value: newValue
    });
    this.props.onChange && this.props.onChange(newValue);
  };

  handleBlur = (event, { newValue }) => {
    if (this.state.suggestions.length == 0) {
      this.setState({ value: "" });
      this.props.formik.form.values[this.props.name] = newValue;
    }
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  validateFunction = () => {
    if (!this.state.validating) return true;
    else return false;
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: this.props.placeholder || "",
      value,
      onChange: this.handleChange,
      onBlur: this.handleBlur,
      label: this.props.label ? this.props.label : ""
    };
    const { classes, formik } = this.props;
    function renderInputComponent(inputProps) {
      const { inputRef = () => {}, ref, ...other } = inputProps;
      return (
        <TextField
          fullWidth
          variant="outlined"
          error={
            formik.form.errors[formik.field.name] &&
            formik.form.touched[formik.field.name]
              ? true
              : false
          }
          helperText={
            formik.form.errors[formik.field.name] &&
            formik.form.touched[formik.field.name]
              ? "Required"
              : ""
          }
          InputProps={{
            inputRef: node => {
              ref(node);
              inputRef(node);
            },
            classes: {
              input: classes.input
            }
          }}
          InputLabelProps={{
            shrink: true
          }}
          {...other}
        />
      );
    }
    const autosuggestProps = {
      renderInputComponent
    };
    return (
      <Autosuggest
        {...autosuggestProps}
        disable={this.props.disabledStatus}
        suggestions={suggestions.slice(0, 10)}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        shouldRenderSuggestions={() => true}
        inputProps={inputProps}
        theme={{
          container: classes.container,
          suggestionsContainer: classes.suggestionsContainer,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestionHighlighted: classes.suggestionHighlighted,
          suggestion: classes.suggestion
        }}
        renderSuggestionsContainer={options => (
          <Paper {...options.containerProps} square>
            {options.children}
          </Paper>
        )}
      />
    );
  }
}

// Docs

// import NewReactAutosuggest from "../../components/NewAutocomplete.js";

// <NewReactAutosuggest
//     name={e.name}
//     key={i + e.name}
//     formik={props}
//     data={e.suggestions}
//     label={e.label}
//     value={values[e.name]}
// />

export default withStyles(styles)(ReactAutosuggest);
