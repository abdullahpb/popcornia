import { combineReducers } from "redux";

import importBill from "./importBill/reducer";

const reducers = combineReducers({
  importBill
});

export default reducers;
