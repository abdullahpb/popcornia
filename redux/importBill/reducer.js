import { FORM_TYPE, ITEM_TYPE } from "../../constant/actionTypes";

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FORM_TYPE:
      return Object.assign({}, state, {
        formType: action.payload
      });

    case "ITEM_TYPE":
      return Object.assign({}, state, {
        itemType: action.payload
      });

    case "BUFFER_DATA":
      //state.allInvoice.push(action.payload);
      return Object.assign({}, state, {
        bufferData: action.payload
      });

    case "SELECTED_ITEM":
      return Object.assign({}, state, {
        itemType: "ITEM_EDIT",
        selectedItem: action.payload
      });

    case "GET_ALL_INVOICE":
      let submittedInvoices = action.payload.filter(
        e => e.formStatus == "SUBMITTED"
      );
      let openInvoices = action.payload.filter(
        e => e.formStatus != "SUBMITTED"
      );
      // let postedInvoices = [];
      return Object.assign({}, state, {
        submittedInvoices: submittedInvoices,
        openInvoices: openInvoices
      });

    case "FORM_SUBMIT_TYPE":
      return Object.assign({}, state, {
        formSubmitType: action.payload
      });

    default:
      return { ...state };
  }
};

let INIT_STATE = {
  formType: "FORM_ADD",
  itemType: "ITEM_ADD",
  bufferData: {},
  selectedItem: {},
  allInvoice: [],
  submittedInvoices: [],
  openInvoices: [],
  formSubmitType: "OPEN"
};
