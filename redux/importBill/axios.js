import axios from "axios";
import { func } from "prop-types";

//const url = "http://134.209.150.237/popcornapi/";
const url = "http://localhost:3000";

export function addImportInvoice(data, bufferData, callback) {
  return axios({
    method: "post",
    url: `${url}/basic`,
    data
  })
    .then(res => {
      let doc = { ...bufferData };
      doc.id = res.data._id;
      callback(doc);
    })
    .catch(err => console.log(err));
}

export function updateImportInvoice(data) {
  axios({
    method: "put",
    url: `${url}/basic`,
    data
  })
    .then(res => console.log(res))
    .catch(err => console.log(err));
}

export function deleteImportInvoice(data, callback) {
  axios({
    method: "delete",
    url: `${url}/basic`,
    data
  })
    .then(() => getAllInvoice(callback))
    .catch(err => console.log(err));
}

export function getAllInvoice(callback) {
  axios
    .get(`${url}/basic`)
    .then(res => {
      console.log(res);
      callback(res.data);
    })
    .catch(err => console.log("dd", err));
}

export function searchInvoice(data, callback) {
  return axios({
    method: "post",
    url: `${url}/search`,
    data
  })
    .then(res => {
      console.log(res.data);
      callback(res.data);
    })
    .catch(err => console.log(err));
}
