import { FORM_TYPE, INVOICE_TYPE } from "../../constant/actionTypes";

export const ChangeFormType = payload => {
  return {
    type: FORM_TYPE,
    payload
  };
};

export const ChangeItemType = payload => {
  return {
    type: "ITEM_TYPE",
    payload
  };
};

export const BufferData = payload => {
  console.log("bufff");
  return {
    type: "BUFFER_DATA",
    payload
  };
};

export const SelectedItem = payload => {
  return {
    type: "SELECTED_ITEM",
    payload
  };
};

export const GetAllInvoice = payload => {
  return {
    type: "GET_ALL_INVOICE",
    payload
  };
};

export const FormSubmitType = payload => {
  return {
    type: "FORM_SUBMIT_TYPE",
    payload
  };
};
