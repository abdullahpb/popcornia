import React from "react";
import { Container, Link, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
}));

function SignIn() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            PROJECT CORN
          </Typography>
          <Link href="/login" variant="body2">
            {"Click to Login"}
          </Link>
        </div>
      </Container>
    </React.Fragment>
  );
}

export default SignIn;
