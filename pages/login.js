import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    paddingTop: "50px",
    paddingBottom: "50px"
  },
  image: {
    backgroundImage:
      "url(https://images.unsplash.com/photo-1531171000775-75f0213ca8a0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  images: {
    backgroundImage:
      "url(http://www.ie-wallpapers.com/data/out/34/36304362-blur-images.jpg)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    width: "20%",
    margin: theme.spacing(3, 0, 2),
    borderRadius: 25,
    lineHeight: 2.75,
    fontSize: "0.65rem"
  },
  textField: {
    [`& fieldset`]: {
      borderRadius: "30px",
      boxShadow: "0 1px 11px 0 rgba(0,0,0,0.008), 0 3px 15px 0 rgba(0,0,0,.05)"
    },
    [`& label`]: {
      fontSize: "13px"
    }
  }
}));

export default function SignInSide() {
  const classes = useStyles();

  return (
    <Grid container style={{ backgroundColor: "#92b8cd", height: "100vh" }}>
      <Grid
        container
        justify="center"
        component="main"
        className={classes.root}
      >
        <CssBaseline />
        <Grid item xs={false} sm={2} md={5} className={classes.image} />
        <Grid
          item
          xs={12}
          sm={8}
          md={5}
          style={{ borderRadius: "50px" }}
          component={Paper}
          elevation={6}
          square
          direction="column"
          alignItems="center"
          justify="center"
        >
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography
              component="h1"
              variant="h5"
              style={{ fontWeight: "1000" }}
            >
              Sign In
            </Typography>
            <form className={classes.form} noValidate>
              <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  id="email"
                  label="Email"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  className={classes.textField}
                  inputProps={{ style: { textAlign: "center", width: 250 } }}
                />
                <br />
                <TextField
                  variant="outlined"
                  margin="normal"
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  className={classes.textField}
                  inputProps={{ style: { textAlign: "center", width: 250 } }}
                />
              </Grid>
              <div style={{ textAlign: "center" }}>
                <Button
                  type="submit"
                  variant="outlined"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  href="/index"
                  style={{ boxShadow: "0 4px 12px 0 rgb(85, 108, 214)" }}
                >
                  Sign In
                </Button>
              </div>

              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Create account"}
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Grid>
      </Grid>
    </Grid>
  );
}
