import React from "react";
import App, { Container } from "next/app";
import Head from "next/head";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../src/theme";
import withReduxStore from "../lib/with-redux-store";
import { Provider } from "react-redux";
import { SnackbarProvider } from "notistack";

import Header from "../components/header";

import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

class MyApp extends App {
  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    return (
      <Container>
        <Provider store={reduxStore}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <SnackbarProvider maxSnack={3}>
              <Head>
                <title>Project Corn</title>
              </Head>
              <ThemeProvider theme={theme}>
                <CssBaseline />
                <Header>
                  <Component {...pageProps} />
                </Header>
              </ThemeProvider>
            </SnackbarProvider>
          </MuiPickersUtilsProvider>
        </Provider>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);
