import React, { Component } from "react";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Divider,
  Grid,
  Paper,
  MenuItem,
  Radio,
  TableCell,
  TableRow,
  TableFooter,
  RadioGroup,
  FormControlLabel
} from "@material-ui/core";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { Formik, Field, Form } from "formik";
// const Field = dynamic(() => import("../../components/formik"), {
//   ssr: false
// });
import { TextField } from "formik-material-ui";
import * as Yup from "yup";

import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import { Create, Delete, ArrowForward, ArrowBack } from "@material-ui/icons";

import MaterialTable, { MTableBody } from "material-table";
import DatePickerMM from "../../components/datepicker-mmddyyy";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { withSnackbar } from "notistack";

import NewAutoComplete from "../../components/newAutoComplete";
import CountryJson from "../../assets/json/countryJson.json";
import SupplierJson from "../../assets/json/supplierJson.json";
const ItemJson = [
  {
    label: "Gold Corn",
    value: "GC"
  },
  {
    label: "Silver Corn",
    value: "SC"
  }
];

const PartyJson = [
  {
    label: "ABC Clearing",
    value: "abcClearing"
  },
  {
    label: "GHJ Insurance",
    value: "ghjInsurance"
  },
  {
    label: "All Freight",
    value: "allFreight"
  }
];

const CurrencyJson = [
  {
    label: "INR",
    value: "inr"
  },
  {
    label: "USD",
    value: "usd"
  },
  {
    label: "EURO",
    value: "euro"
  },
  {
    label: "YEN",
    value: "yen"
  }
];

const CreditJson = [
  {
    label: "B/L",
    value: "b/l"
  },
  {
    label: "INV",
    value: "inv"
  }
];

const WeightJson = [
  {
    label: "Lbs",
    value: "lbs"
  },
  {
    label: "Kgs",
    value: "kgs"
  }
];

const OtherItemJson = [
  {
    label: "Insurance",
    value: "insurance"
  },
  {
    label: "Frieght",
    value: "frieght"
  },
  {
    label: "Clearing",
    value: "clearing"
  }
];

import { connect } from "react-redux";
import * as actionCreator from "../../redux/actions";
import * as apiCallCreator from "../../redux/importBill/axios";
import { throws } from "assert";
import ImportSearch from "../../components/importSearch";
import { withStyles } from "@material-ui/core/styles";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = {
  root: {
    backgroundColor: "transparent"
  },

  paper: {
    backgroundColor: "transparent",
    boxShadow: "none",
    overflow: "hidden"
  },
  dialogPaper: {
    minHeight: "87vh",
    maxHeight: "87vh",
    position: "absolute",
    bottom: 0,
    left: 0
  }
};

class ImportBill extends Component {
  state = {
    searchType: "",
    submitType: "OPEN",
    navigation: false,
    navigationSide: "FORWARD",
    currentInvoice: {},
    currentItem: {},
    form1Data: [],
    form2Data: [],
    form3Data: [],
    form4Data: [],
    searchTabContainer: 0,
    formTabContainer: 0,
    formStatus: "FORM_ADD",
    invoiceFormState: false,
    supplierInvoiceItemState: false,
    billofentryInvoiceItemState: false,
    otherchargesInvoiceItemState: false,
    journalInvoiceItemState: false,
    invoiceItemState: false,
    searchTransaction: "DATE",
    searchInvoice: "DATE",
    selectedRows: []
  };

  componentWillMount() {
    apiCallCreator.getAllInvoice(this.props.getAllInvoice);
  }

  handleTabChange = (event, val) => {
    // if (formState == "FORM_ADD") {
    //   return null;
    // }
    this.setState({ searchTabContainer: val });
  };

  formHandler;

  handleInnerTabChange = val => {
    switch (val) {
      case "FORWARD":
        if (this.state.formTabContainer == 3) return null;
        this.setState({
          formTabContainer: this.state.formTabContainer + 1
        });
        break;
      case "BACKWARD":
        if (this.state.formTabContainer == 0) return null;
        this.setState({
          formTabContainer: this.state.formTabContainer - 1
        });
        break;
    }
  };

  handleDialog = val => {
    switch (val) {
      case "INVOICE_FORM":
        apiCallCreator.getAllInvoice(this.props.getAllInvoice);
        this.setState({ invoiceFormState: !this.state.invoiceFormState });
        break;

      case "INVOICE_ITEM":
        this.setState({ invoiceItemState: !this.state.invoiceItemState });
        break;

      case "SUPPLIER_INVOICE_ITEM":
        this.setState({
          supplierInvoiceItemState: !this.state.supplierInvoiceItemState
        });
        break;

      case "BILLOFENTRY_INVOICE_ITEM":
        this.setState({
          billofentryInvoiceItemState: !this.state.billofentryInvoiceItemState
        });
        break;

      case "OTHERCHARGES_INVOICE_ITEM":
        this.setState({
          otherchargesInvoiceItemState: !this.state.otherchargesInvoiceItemState
        });
        break;

      case "JOURNAL_INVOICE_ITEM":
        this.setState({
          journalInvoiceItemState: !this.state.journalInvoiceItemState
        });
        break;
    }
  };

  render() {
    const { classes } = this.props;
    let importBill = this.props.importBill;
    console.log(importBill);
    return (
      <React.Fragment>
        <Paper style={{ marginTop: "10px", minHeight: "600px" }}>
          <TabContainer>
            <Grid container style={{ margin: 0, padding: 0 }}>
              <Grid item xs={6}>
                <Typography variant="overline" style={{ color: "#424242" }}>
                  SEARCH PANEL
                </Typography>
              </Grid>
              <Grid item xs={6} style={{ textAlign: "right" }}>
                <Button
                  variant="outlined"
                  color="primary"
                  size="small"
                  style={{
                    width: "200px",
                    marginBottom: "5px",
                    borderRadius: "25px"
                  }}
                  onClick={() => {
                    this.props.bufferData({});
                    this.props.formType("FORM_ADD");
                    this.setState({
                      form1Data: [],
                      form2Data: [],
                      form3Data: [],
                      form4Data: []
                    });
                    this.handleDialog("INVOICE_FORM");
                  }}
                >
                  Add New
                </Button>
              </Grid>
            </Grid>
            <Divider />
            <Grid
              container
              style={{
                marginTop: 5,
                padding: 10
              }}
            >
              <Grid item xs={6} style={{ padding: 5, height: 150 }}>
                <ImportSearch />
              </Grid>
            </Grid>
            <Tabs
              value={this.state.searchTabContainer}
              onChange={this.handleTabChange}
            >
              <Tab label="Open" />
              <Tab label="Submitted" />
              <Tab label="Posted" />
            </Tabs>
            {this.state.searchTabContainer === 0 && (
              <TabContainer>
                <MaterialTable
                  title="Search Result - Open"
                  columns={columnsSearch}
                  data={this.props.importBill.openInvoices}
                  options={{
                    sort: false,
                    search: false,
                    paging: false
                    // selection: true
                  }}
                  style={{ marginBottom: "10px" }}
                  onSelectionChange={rows => {
                    if (rows[0] == null || rows[0] == undefined) {
                      return 0;
                    }
                    this.props.bufferData(rows[0]);
                    this.props.formType("FORM_VIEW");
                  }}
                  actions={[
                    {
                      icon: Create,
                      onClick: (event, rowData) => {
                        this.setState({
                          form1Data: rowData.supplierInvoice,
                          form2Data: rowData.billOfEntry,
                          form3Data: rowData.otherCharges,
                          form4Data: rowData.journal
                        });
                        this.props.bufferData(rowData);
                        this.props.formType("FORM_VIEW");
                        this.handleDialog("INVOICE_FORM");
                      }
                    },
                    {
                      icon: Delete,
                      onClick: (event, rowData) => {
                        apiCallCreator.deleteImportInvoice(
                          rowData,
                          this.props.getAllInvoice
                        );
                        this.props.enqueueSnackbar(
                          `${rowData.transactionNo} Deleted Successfully`,
                          { variant: "success" }
                        );
                      }
                    }
                  ]}
                  components={{
                    Toolbar: () => {
                      return (
                        <Grid
                          container
                          direction="row"
                          justify="flex-end"
                          alignItems="center"
                        >
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              this.setState({
                                form1Data: [],
                                form2Data: [],
                                form3Data: [],
                                form4Data: []
                              });
                              this.props.formType("FORM_ADD");
                              this.props.bufferData({});
                              this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            + Add New Docs
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              // this.props.formType("FORM_VIEW");
                              // this.setState({
                              //   form1Data:
                              //     importBill.bufferData.supplierInvoice,
                              //   form2Data: importBill.bufferData.billOfEntry,
                              //   form3Data: importBill.bufferData.otherCharges,
                              //   form4Data: importBill.bufferData.journal
                              // });
                              // this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            Open Doc
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() =>
                              apiCallCreator.deleteImportInvoice(
                                importBill.bufferData,
                                this.props.getAllInvoice
                              )
                            }
                          >
                            Delete Docs
                          </Button>
                        </Grid>
                      );
                    }
                  }}
                />
              </TabContainer>
            )}
            {this.state.searchTabContainer === 1 && (
              <TabContainer>
                <MaterialTable
                  title="Search Result - Submitted"
                  columns={columnsSearch}
                  data={this.props.importBill.submittedInvoices}
                  options={{
                    sort: false,
                    search: false,
                    paging: false
                    // selection: true
                  }}
                  style={{ marginBottom: "10px" }}
                  onSelectionChange={rows => {
                    if (rows[0] == null || rows[0] == undefined) {
                      return 0;
                    }
                    this.props.bufferData(rows[0]);
                    this.props.formType("FORM_VIEW");
                  }}
                  actions={[
                    {
                      icon: Create,
                      onClick: (event, rowData) => {
                        this.setState({
                          form1Data: rowData.supplierInvoice,
                          form2Data: rowData.billOfEntry,
                          form3Data: rowData.otherCharges,
                          form4Data: rowData.journal
                        });
                        this.props.bufferData(rowData);
                        this.props.formType("FORM_VIEW");
                        this.handleDialog("INVOICE_FORM");
                      }
                    },
                    {
                      icon: Delete,
                      onClick: (event, rowData) => {
                        apiCallCreator.deleteImportInvoice(
                          rowData,
                          this.props.getAllInvoice
                        );
                        this.props.enqueueSnackbar(
                          `${rowData.transactionNo} Deleted Successfully`,
                          { variant: "success" }
                        );
                      }
                    }
                  ]}
                  components={{
                    Toolbar: () => {
                      return (
                        <Grid
                          container
                          direction="row"
                          justify="flex-end"
                          alignItems="center"
                        >
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              this.setState({
                                form1Data: [],
                                form2Data: [],
                                form3Data: [],
                                form4Data: []
                              });
                              this.props.formType("FORM_ADD");
                              this.props.bufferData({});
                              this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            + Add New Docs
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              // this.props.formType("FORM_VIEW");
                              // this.setState({
                              //   form1Data:
                              //     importBill.bufferData.supplierInvoice,
                              //   form2Data: importBill.bufferData.billOfEntry,
                              //   form3Data: importBill.bufferData.otherCharges,
                              //   form4Data: importBill.bufferData.journal
                              // });
                              // this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            Open Doc
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() =>
                              apiCallCreator.deleteImportInvoice(
                                importBill.bufferData,
                                this.props.getAllInvoice
                              )
                            }
                          >
                            Delete Docs
                          </Button>
                        </Grid>
                      );
                    }
                  }}
                />
              </TabContainer>
            )}
            {this.state.searchTabContainer === 2 && (
              <TabContainer>
                <MaterialTable
                  title="Search Result - Posted"
                  columns={columnsSearch}
                  data={this.props.importBill.submittedInvoices}
                  options={{
                    sort: false,
                    search: false,
                    paging: false,
                    selection: true
                  }}
                  style={{ marginBottom: "10px" }}
                  onSelectionChange={rows => {
                    if (rows[0] == null || rows[0] == undefined) {
                      return 0;
                    }
                    this.props.bufferData(rows[0]);
                    this.props.formType("FORM_VIEW");
                  }}
                  components={{
                    Toolbar: () => {
                      return (
                        <Grid
                          container
                          direction="row"
                          justify="flex-end"
                          alignItems="center"
                        >
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              this.setState({
                                form1Data: [],
                                form2Data: [],
                                form3Data: [],
                                form4Data: []
                              });
                              this.props.formType("FORM_ADD");
                              this.props.bufferData({});
                              this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            + Add New Docs
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              // this.props.formType("FORM_VIEW");
                              // this.setState({
                              //   form1Data:
                              //     importBill.bufferData.supplierInvoice,
                              //   form2Data: importBill.bufferData.billOfEntry,
                              //   form3Data: importBill.bufferData.otherCharges,
                              //   form4Data: importBill.bufferData.journal
                              // });
                              // this.handleDialog("INVOICE_FORM");
                            }}
                          >
                            Open Doc
                          </Button>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() =>
                              apiCallCreator.deleteImportInvoice(
                                importBill.bufferData,
                                this.props.getAllInvoice
                              )
                            }
                          >
                            Delete Docs
                          </Button>
                        </Grid>
                      );
                    }
                  }}
                />
              </TabContainer>
            )}
          </TabContainer>
        </Paper>

        {/*************************SUPPLIER ITEM INVOICE DIALOG STARTS *************************/}
        <Dialog
          open={this.state.supplierInvoiceItemState}
          onClose={() => this.handleDialog("SUPPLIER_INVOICE_ITEM")}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Invoice Line Item</DialogTitle>
          <DialogContent>
            <Formik
              initialValues={{
                id:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.id
                    : this.state.form1Data.length + 1,
                item:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.item
                    : "",
                qty:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.qty
                    : "",
                uom:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.uom
                    : "lbs",
                packSize:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.packSize
                    : "",
                packUom:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.packUom
                    : "lbs",
                unitPrice:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.unitPrice
                    : ""
              }}
              validationSchema={supplierSchema}
              onSubmit={(values, { setSubmitting }) => {
                if (importBill.itemType == "ITEM_ADD") {
                  this.state.form1Data.push(values);
                }
                if (importBill.itemType == "ITEM_EDIT") {
                  let form1Data = [...this.state.form1Data];
                  let index = this.state.form1Data.indexOf(
                    importBill.selectedItem
                  );
                  form1Data[index] = values;
                  this.setState({ form1Data });
                }
                this.handleDialog("SUPPLIER_INVOICE_ITEM");
                setSubmitting(false);
              }}
            >
              {({ values }) => (
                <Form autoComplete="off">
                  <Grid container spacing={0}>
                    <Grid item xs={12}>
                      <Field
                        name="item"
                        autoFocus
                        render={props => {
                          return (
                            <NewAutoComplete
                              name="item"
                              style={{ width: "95%", marginTop: "16px" }}
                              formik={props}
                              data={ItemJson}
                              label="Item Name"
                              value={values.item}
                            />
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="qty"
                        label="Qty"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="uom"
                        style={{ width: "95%", marginBottom: "10px" }}
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px" }}>
                              <NewAutoComplete
                                name="uom"
                                formik={props}
                                data={WeightJson}
                                label="UOM"
                                value={values.uom}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="packSize"
                        label="Pack Size"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="packUom"
                        style={{ width: "95%", marginBottom: "10px" }}
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px" }}>
                              <NewAutoComplete
                                name="packUom"
                                formik={props}
                                data={WeightJson}
                                label="Pack UOM"
                                value={values.packUom}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="unitPrice"
                        label="Unit Price"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        onClick={() =>
                          this.handleDialog("SUPPLIER_INVOICE_ITEM")
                        }
                        color="primary"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" color="primary">
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </DialogContent>
        </Dialog>
        {/*************************SUPPLIER ITEM INVOICE DIALOG ENDS *************************/}

        {/*************************BILL OF ENTRY ITEM INVOICE DIALOG STARTS *************************/}
        <Dialog
          open={this.state.billofentryInvoiceItemState}
          onClose={() => this.handleDialog("BILLOFENTRY_INVOICE_ITEM")}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Invoice Line Item</DialogTitle>
          <DialogContent>
            <Formik
              initialValues={{
                id:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.id
                    : "",
                item:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.item
                    : "",
                qty:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.qty
                    : "",
                uom:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.uom
                    : "lbs",
                unitPrice:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.unitPrice
                    : "",
                duty:
                  importBill.itemType == "ITEM_EDIT" &&
                  importBill.formType != "FORM_ADD"
                    ? importBill.selectedItem.duty
                    : ""
              }}
              validationSchema={boiSchema}
              onSubmit={(values, { setSubmitting }) => {
                if (importBill.itemType == "ITEM_ADD") {
                  this.state.form2Data.push(values);
                }
                if (importBill.itemType == "ITEM_EDIT") {
                  let form2Data = [...this.state.form2Data];
                  let index = this.state.form2Data.indexOf(
                    importBill.selectedItem
                  );
                  form2Data[index] = values;
                  this.setState({ form2Data });
                }

                this.handleDialog("BILLOFENTRY_INVOICE_ITEM");
                setSubmitting(false);
              }}
            >
              {({ values }) => (
                <Form autoComplete="off">
                  <Grid container spacing={0}>
                    <Grid item xs={12}>
                      <Field
                        name="item"
                        autoFocus
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px" }}>
                              <NewAutoComplete
                                name="item"
                                style={{ width: "95%", marginTop: "16px" }}
                                formik={props}
                                data={ItemJson}
                                label="Item Name"
                                value={values.item}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="qty"
                        label="Qty"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="uom"
                        style={{ width: "95%", marginBottom: "10px" }}
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px" }}>
                              <NewAutoComplete
                                name="uom"
                                style={{ width: "95%", marginTop: "16px" }}
                                formik={props}
                                data={WeightJson}
                                label="UOM"
                                value={values.uom}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="unitPrice"
                        label="Unit Price"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="duty"
                        label="Duty"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        onClick={() =>
                          this.handleDialog("BILLOFENTRY_INVOICE_ITEM")
                        }
                        color="primary"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" color="primary">
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </DialogContent>
        </Dialog>
        {/*************************BILL OF ITEM INVOICE DIALOG ENDS *************************/}

        {/*************************OTHER CHARGES ITEM INVOICE DIALOG STARTS *************************/}
        <Dialog
          open={this.state.otherchargesInvoiceItemState}
          onClose={() => this.handleDialog("OTHERCHARGES_INVOICE_ITEM")}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Invoice Line Item</DialogTitle>
          <DialogContent>
            <Formik
              initialValues={{
                party:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.party
                    : "",
                invoiceNo:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.invoiceNo
                    : "",
                date:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.date
                    : "",
                item:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.item
                    : "",
                amount:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.amount
                    : "",
                taxes:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.taxes
                    : "",
                total:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.total
                    : ""
              }}
              validationSchema={otherSchema}
              onSubmit={(values, { setSubmitting }) => {
                if (importBill.itemType == "ITEM_ADD") {
                  this.state.form3Data.push(values);
                }
                if (importBill.itemType == "ITEM_EDIT") {
                  let form3Data = [...this.state.form3Data];
                  let index = this.state.form3Data.indexOf(
                    importBill.selectedItem
                  );
                  form3Data[index] = values;
                  this.setState({ form3Data });
                }
                this.handleDialog("OTHERCHARGES_INVOICE_ITEM");
                setSubmitting(false);
              }}
            >
              {({ values }) => (
                <Form autoComplete="off">
                  <Grid container spacing={0}>
                    <Grid item xs={12}>
                      <Field
                        name="party"
                        autoFocus
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px" }}>
                              <NewAutoComplete
                                style={{ width: "95%", marginTop: "16px" }}
                                name="party"
                                formik={props}
                                data={PartyJson}
                                label="Party"
                                value={values.party}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="invoiceNo"
                        label="Invoice #"
                        type="text"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="date"
                        inputVariant="outlined"
                        render={props => {
                          return (
                            <DatePickerMM
                              initialValue={values.date}
                              name="date"
                              label="Date"
                              formik={props.form}
                            />
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="item"
                        render={props => {
                          return (
                            <div style={{ marginTop: "16px", width: "95%" }}>
                              <NewAutoComplete
                                name="item"
                                formik={props}
                                data={OtherItemJson}
                                label="Item Name"
                                value={values.item}
                              />
                            </div>
                          );
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        type="number"
                        name="amount"
                        label="Amount"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        margin="normal"
                        component={TextField}
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="taxes"
                        label="Taxes"
                        type="number"
                        style={{ width: "95%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        onClick={() =>
                          this.handleDialog("OTHERCHARGES_INVOICE_ITEM")
                        }
                        color="primary"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" color="primary">
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </DialogContent>
        </Dialog>
        {/*************************OTHER CHARGES ITEM INVOICE DIALOG ENDS *************************/}

        {/*************************JOURNAL ITEM INVOICE DIALOG STARTS *************************/}
        <Dialog
          open={this.state.journalInvoiceItemState}
          onClose={() => this.handleDialog("JOURNAL_INVOICE_ITEM")}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Invoice Line Item</DialogTitle>
          <DialogContent>
            <Formik
              initialValues={{
                accountHead:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.accountHead
                    : "",
                credit:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.credit
                    : "",
                debit:
                  importBill.itemType == "ITEM_EDIT"
                    ? importBill.selectedItem.debit
                    : ""
              }}
              validationSchema={journalSchema}
              onSubmit={(values, { setSubmitting }) => {
                if (importBill.itemType == "ITEM_ADD") {
                  this.state.form4Data.push(values);
                }
                if (importBill.itemType == "ITEM_EDIT") {
                  let form4Data = [...this.state.form4Data];
                  let index = this.state.form4Data.indexOf(
                    importBill.selectedItem
                  );
                  form4Data[index] = values;
                  this.setState({ form4Data });
                }
                this.handleDialog("JOURNAL_INVOICE_ITEM");
                setSubmitting(false);
              }}
            >
              {({ values }) => (
                <Form autoComplete="off">
                  <Grid container spacing={0}>
                    <Grid item xs={12}>
                      <Field
                        name="accountHead"
                        label="Account Head"
                        autoFocus
                        type="text"
                        value={values.accountHead}
                        style={{ width: "98%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="credit"
                        label="Credit"
                        type="number"
                        value={values.credit}
                        style={{ width: "98%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Field
                        name="debit"
                        label="Debit"
                        type="number"
                        value={values.debit}
                        style={{ width: "98%", marginBottom: "10px" }}
                        variant="outlined"
                        component={TextField}
                        margin="normal"
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        onClick={() =>
                          this.handleDialog("JOURNAL_INVOICE_ITEM")
                        }
                        color="primary"
                      >
                        Cancel
                      </Button>
                      <Button type="submit" color="primary">
                        Submit
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </DialogContent>
        </Dialog>
        {/*************************JOURNAL ITEM INVOICE DIALOG ENDS *************************/}

        {/************************* FORM INVOICE DIALOG STARTS *************************/}
        <Dialog
          open={this.state.invoiceFormState}
          classes={{
            paper: classes.dialogPaper
          }}
          BackdropProps={{
            classes: {
              root: classes.root
            }
          }}
          PaperProps={{
            classes: {
              root: classes.paper
            }
          }}
          fullScreen
          onClose={() => {
            this.handleDialog("INVOICE_FORM");
          }}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title" style={{ padding: 0 }}>
            <Paper square={true} style={{ flexGrow: 1 }}>
              <Grid container style={{ padding: 10 }}>
                <Grid item style={{ width: "50%", textAlign: "left" }}>
                  <IconButton
                    onClick={() => {
                      this.setState({
                        navigation: true,
                        navigationSide: "BACKWARD"
                      });
                      this.formHandler();
                    }}
                    aria-label="Forward"
                    size="small"
                  >
                    <ArrowBack fontSize="inherit" />
                  </IconButton>
                  <IconButton
                    onClick={() => {
                      this.setState({
                        navigation: true,
                        navigationSide: "FORWARD"
                      });
                      this.formHandler();
                    }}
                    aria-label="Back"
                    size="small"
                  >
                    <ArrowForward fontSize="inherit" />
                  </IconButton>
                  <Chip
                    label={`Form Status : ${importBill.bufferData.formStatus ||
                      "OPEN"}`}
                    color="primary"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={6} style={{ width: "50%", textAlign: "right" }}>
                  <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={() => {
                      this.handleDialog("INVOICE_FORM");
                    }}
                    style={{
                      margin: 0,
                      width: "100px",
                      borderRadius: "25px"
                    }}
                  >
                    cancel
                  </Button>{" "}
                  <Button
                    onClick={() => {
                      this.setState({ submitType: "SAVE" });
                      this.formHandler();
                    }}
                    variant="outlined"
                    color="primary"
                    size="small"
                    style={{
                      margin: 0,
                      width: "100px",
                      borderRadius: "25px"
                    }}
                  >
                    Save
                  </Button>{" "}
                  <Button
                    // type="submit"
                    variant="outlined"
                    color="primary"
                    size="small"
                    style={{
                      margin: 0,
                      width: "100px",
                      borderRadius: "25px"
                    }}
                    onClick={() => {
                      this.setState({ submitType: "SUBMITTED" });
                      this.formHandler();
                    }}
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
              <Tabs
                value={this.state.formTabContainer}
                onChange={(evt, val) => {
                  // if (importBill.formType == "FORM_ADD") {
                  //   return null;
                  // }
                  // this.setState({ formTabContainer: val });
                }}
              >
                <Tab label="Supplier Invoice" />
                <Tab label="Bill of Entry" />
                <Tab label="Other Charges" />
                <Tab label="Accounting Journal" />
              </Tabs>
            </Paper>
          </DialogTitle>
          <DialogContent style={{ margin: 0, padding: 0 }}>
            <Paper style={{ minHeight: "600px" }}>
              {this.state.formTabContainer === 0 && (
                <TabContainer>
                  <Formik
                    initialValues={{
                      transactionNo:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.transactionNo
                          : "",
                      transactionDate:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.transactionDate
                          : new Date().toISOString(),
                      supplier:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.supplier
                          : "",
                      countryOfOrigin:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.countryOfOrigin
                          : "",
                      currency:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.currency
                          : "",
                      rate:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.rate
                          : "",
                      lCurrency:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.lCurrency
                          : "INR",
                      supplierInvoiceNo:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.supplierInvoiceNo
                          : "",
                      invoiceDate:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.invoiceDate
                          : "",
                      poReference:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.poReference
                          : "",
                      creditDays:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.creditDays
                          : "",
                      creditFrom:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.creditFrom
                          : "",
                      bl_awbNo:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.bl_awbNo
                          : "",
                      bl_awbDate:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.bl_awbDate
                          : "",
                      boeNo:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.boeNo
                          : "",
                      boeDate:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.boeDate
                          : "",
                      insurance:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.insurance
                          : "",
                      freight:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.freight
                          : "",
                      total:
                        importBill.formType != "FORM_ADD"
                          ? importBill.bufferData.header.total
                          : ""
                    }}
                    validationSchema={schema}
                    onSubmit={(values, { setSubmitting }) => {
                      if (importBill.formType == "FORM_ADD") {
                        if (this.state.submitType == "SUBMITTED") {
                          values.transactionNo = `IMP/001/1906/0000${this.props
                            .importBill.submittedInvoices.length + 1}`;
                          values.formStatus = "SUBMITTED";
                        } else {
                          values.transactionNo = `TEMP/0000${this.props
                            .importBill.openInvoices.length + 1}`;
                          values.formStatus = "OPEN";
                        }
                        values.narration = "";
                        let fcAmt = 0;
                        this.state.form1Data.forEach(e => {
                          let total = e.qty * e.unitPrice;
                          fcAmt += total;
                        });
                        this.state.form1Data.forEach(e =>
                          this.state.form2Data.push({
                            item: e.item,
                            qty: e.qty,
                            uom: "kgs",
                            unitPrice: e.unitPrice * values.rate,
                            total: e.qty * e.unitPrice,
                            duty: 0
                          })
                        );
                        let currentInvoice = {
                          transactionNo: values.transactionNo,
                          transactionDate: values.transactionDate,
                          formStatus: values.formStatus,
                          supplier: values.supplier,
                          invoiceNo: values.supplierInvoiceNo,
                          invoiceDate: values.invoiceDate,
                          fc: values.currency,
                          fcAmt: fcAmt,
                          duty: 0,
                          other: 0,
                          header: values,
                          supplierInvoice: this.state.form1Data,
                          billOfEntry: this.state.form2Data,
                          otherCharges: [],
                          journal: []
                        };
                        this.props.bufferData(currentInvoice);
                        if (!this.state.navigation) {
                          apiCallCreator.addImportInvoice(
                            currentInvoice,
                            currentInvoice,
                            this.props.bufferData
                          );
                          this.props.enqueueSnackbar(
                            `${
                              currentInvoice.transactionNo
                            } Added Successfully`,
                            { variant: "success" }
                          );
                        }
                      }
                      if (importBill.formType == "FORM_VIEW") {
                        if (
                          this.state.submitType == "SUBMITTED" &&
                          importBill.bufferData.formStatus == "OPEN"
                        ) {
                          values.transactionNo = `IMP/001/1906/0000${this.props
                            .importBill.submittedInvoices.length + 1}`;
                          values.formStatus = "SUBMITTED";
                        } else {
                          values.transactionNo =
                            importBill.bufferData.transactionNo;
                          values.formStatus = importBill.bufferData.formStatus;
                        }
                        values.narration =
                          importBill.bufferData.header.narration;
                        let fcAmt = 0;
                        this.state.form1Data.forEach(e => {
                          let total = e.qty * e.unitPrice;
                          fcAmt += total;
                        });
                        let form2Data = [];
                        this.state.form1Data.forEach((e, i) => {
                          let duty = 0;
                          if (!this.state.form2Data[i]) {
                            duty = 0;
                          } else {
                            duty = this.state.form2Data[i].duty;
                          }
                          form2Data.push({
                            item: e.item,
                            qty: e.qty,
                            uom: "kgs",
                            unitPrice: e.unitPrice * values.rate,
                            total: e.qty * e.unitPrice,
                            duty: duty
                          });
                        });
                        this.setState({ form2Data });
                        let currentInvoice = {
                          id: importBill.bufferData._id,
                          transactionNo: values.transactionNo,
                          transactionDate: values.transactionDate,
                          formStatus: values.formStatus,
                          supplier: values.supplier,
                          invoiceNo: values.supplierInvoiceNo,
                          invoiceDate: values.invoiceDate,
                          fc: values.currency,
                          fcAmt: fcAmt,
                          duty: 0,
                          other: 0,
                          header: values,
                          supplierInvoice: this.state.form1Data,
                          billOfEntry: importBill.bufferData.billOfEntry,
                          otherCharges: importBill.bufferData.otherCharges,
                          journal: importBill.bufferData.journal
                        };
                        this.props.bufferData(currentInvoice);
                        if (!this.state.navigation) {
                          apiCallCreator.updateImportInvoice(currentInvoice);
                          this.props.enqueueSnackbar(
                            `${
                              currentInvoice.transactionNo
                            } Added Successfully`,
                            { variant: "success" }
                          );
                        }
                      }
                      if (!this.state.navigation) {
                        this.handleDialog("INVOICE_FORM");
                      }
                      if (this.state.navigation) {
                        this.handleInnerTabChange(this.state.navigationSide);
                      }
                      this.setState({ navigation: false });
                      setSubmitting(false);
                    }}
                  >
                    {({ values, handleSubmit }) => {
                      this.formHandler = handleSubmit;
                      return (
                        <Form autoComplete="off">
                          <Grid container spacing={0}>
                            {importBill.formType != "FORM_ADD" ? (
                              <Grid item xs={3}>
                                <Field
                                  name="transactionNo"
                                  label="Transaction No"
                                  component={TextField}
                                  value={values.transactionNo}
                                  style={{ width: "95%", marginBottom: "10px" }}
                                  variant="outlined"
                                  margin="normal"
                                  disabled
                                  InputLabelProps={{
                                    shrink: true
                                  }}
                                />
                              </Grid>
                            ) : (
                              ""
                            )}
                            {fieldMapper(form1, values)}
                            <Grid item style={{ width: "100%" }}>
                              <MaterialTable
                                title="Details"
                                columns={columnsForm1}
                                data={this.state.form1Data}
                                options={{
                                  sort: false,
                                  search: false,
                                  paging: false
                                  // selection: true
                                }}
                                onSelectionChange={rows => {
                                  if (rows[0] == null || rows[0] == undefined) {
                                    this.props.selectedItem({});
                                    return 0;
                                  }
                                  this.setState({ selectedRows: rows });
                                  this.props.selectedItem(rows[0]);
                                }}
                                actions={[
                                  {
                                    icon: Create,
                                    onClick: (event, rowData) => {
                                      this.props.selectedItem(rowData);
                                      this.props.itemType("ITEM_EDIT");
                                      this.handleDialog(
                                        "SUPPLIER_INVOICE_ITEM"
                                      );
                                    }
                                  },
                                  {
                                    icon: Delete,
                                    onClick: (event, rowData) => {
                                      const form1Data = [
                                        ...this.state.form1Data
                                      ];
                                      form1Data.splice(
                                        form1Data.indexOf(rowData),
                                        1
                                      );
                                      this.setState({ form1Data });
                                    }
                                  }
                                ]}
                                components={{
                                  Toolbar: () => {
                                    return (
                                      <Grid
                                        container
                                        direction="row"
                                        justify="flex-end"
                                        alignItems="center"
                                      >
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            this.props.itemType("ITEM_ADD");
                                            this.handleDialog(
                                              "SUPPLIER_INVOICE_ITEM"
                                            );
                                          }}
                                        >
                                          + Add Row
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // this.props.itemType("ITEM_EDIT");
                                            // this.handleDialog(
                                            //   "SUPPLIER_INVOICE_ITEM"
                                            // );
                                          }}
                                        >
                                          Open Form
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // let form1Data = this.state.form1Data.filter(
                                            //   e => {
                                            //     return !this.state.selectedRows.some(
                                            //       s => {
                                            //         if (s.id === e.id)
                                            //           return true;
                                            //       }
                                            //     );
                                            //   }
                                            // );
                                            // this.setState({ form1Data });
                                          }}
                                        >
                                          Delete Row
                                        </Button>
                                      </Grid>
                                    );
                                  },
                                  Body: props => {
                                    let subTotal = 0;
                                    props.renderData.forEach(e => {
                                      let total = e.qty * e.unitPrice;
                                      subTotal += parseInt(total);
                                    });
                                    let total =
                                      subTotal +
                                      values.insurance +
                                      values.freight;
                                    return (
                                      <React.Fragment>
                                        <MTableBody {...props} />
                                        <TableFooter>
                                          <TableRow>
                                            <TableCell
                                              rowSpan={4}
                                              colSpan={6}
                                            />
                                            <TableCell>Sub Total</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {subTotal}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Insurance</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {values.insurance}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>freight</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {values.freight}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Total</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {total}
                                            </TableCell>
                                          </TableRow>
                                        </TableFooter>
                                      </React.Fragment>
                                    );
                                  }
                                }}
                              />
                            </Grid>
                          </Grid>
                        </Form>
                      );
                    }}
                  </Formik>
                </TabContainer>
              )}
              {this.state.formTabContainer === 1 && (
                <TabContainer>
                  <Formik
                    initialValues={{
                      transactionNo: importBill.bufferData.header.transactionNo,
                      transactionDate:
                        importBill.bufferData.header.transactionDate,
                      supplier: importBill.bufferData.header.supplier,
                      countryOfOrigin:
                        importBill.bufferData.header.countryOfOrigin,
                      currency: importBill.bufferData.header.currency,
                      rate: importBill.bufferData.header.rate,
                      lCurrency: importBill.bufferData.header.lCurrency,
                      supplierInvoiceNo:
                        importBill.bufferData.header.supplierInvoiceNo,
                      invoiceDate: importBill.bufferData.header.invoiceDate,
                      poReference: importBill.bufferData.header.poReference,
                      creditDays: importBill.bufferData.header.creditDays,
                      creditFrom: importBill.bufferData.header.creditFrom,
                      bl_awbNo: importBill.bufferData.header.bl_awbNo,
                      bl_awbDate: importBill.bufferData.header.bl_awbDate,
                      boeNo: importBill.bufferData.header.boeNo,
                      boeDate: importBill.bufferData.header.boeDate,
                      insurance: importBill.bufferData.header.insurance,
                      freight: importBill.bufferData.header.freight,
                      total: importBill.bufferData.header.total
                    }}
                    validationSchema={boeSchema}
                    onSubmit={(values, { setSubmitting }) => {
                      if (
                        this.state.submitType == "SUBMITTED" &&
                        importBill.bufferData.formStatus == "OPEN"
                      ) {
                        values.transactionNo = `IMP/001/1906/0000${this.props
                          .importBill.submittedInvoices.length + 1}`;
                        values.formStatus = "SUBMITTED";
                      } else {
                        values.transactionNo =
                          importBill.bufferData.transactionNo;
                        values.formStatus = importBill.bufferData.formStatus;
                      }
                      values.narration = importBill.bufferData.header.narration;
                      let duty = 0;
                      this.state.form2Data.forEach(e => {
                        duty += e.duty;
                      });
                      let currentInvoice = {
                        id: importBill.bufferData._id,
                        transactionNo: values.transactionNo,
                        transactionDate: values.transactionDate,
                        formStatus: values.formStatus,
                        supplier: values.supplier,
                        invoiceNo: values.supplierInvoiceNo,
                        invoiceDate: values.invoiceDate,
                        fc: values.currency,
                        fcAmt: importBill.bufferData.fcAmt,
                        duty: duty,
                        other: 0,
                        header: values,
                        supplierInvoice: importBill.bufferData.supplierInvoice,
                        billOfEntry: this.state.form2Data,
                        otherCharges: importBill.bufferData.otherCharges,
                        journal: importBill.bufferData.journal
                      };
                      this.props.bufferData(currentInvoice);
                      if (!this.state.navigation) {
                        console.log("ssssssssss", this.state.navigation);
                        apiCallCreator.updateImportInvoice(currentInvoice);
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                        this.handleDialog("INVOICE_FORM");
                      }
                      if (
                        currentInvoice.id == undefined &&
                        importBill.formType == "FORM_ADD" &&
                        !this.state.navigation
                      ) {
                        apiCallCreator.addImportInvoice(
                          currentInvoice,
                          currentInvoice,
                          this.props.bufferData
                        );
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                      }
                      if (this.state.navigation) {
                        this.handleInnerTabChange(this.state.navigationSide);
                      }
                      this.setState({ navigation: false });
                      setSubmitting(false);
                    }}
                  >
                    {({ values, handleSubmit }) => {
                      this.formHandler = handleSubmit;
                      return (
                        <Form autoComplete="off">
                          <Grid container spacing={0}>
                            {importBill.formType != "FORM_ADD" ? (
                              <Grid item xs={3}>
                                <Field
                                  name="transactionNo"
                                  label="Transaction No"
                                  component={TextField}
                                  value={values.transactionNo}
                                  style={{ width: "95%", marginBottom: "10px" }}
                                  variant="outlined"
                                  margin="normal"
                                  disabled
                                  InputLabelProps={{
                                    shrink: true
                                  }}
                                />
                              </Grid>
                            ) : (
                              ""
                            )}
                            {fieldMapper(form1, values)}
                            <Grid item style={{ width: "100%" }}>
                              <MaterialTable
                                title="Details"
                                columns={columnsForm2}
                                data={this.state.form2Data}
                                options={{
                                  sort: false,
                                  search: false,
                                  paging: false
                                  //selection: true
                                }}
                                onSelectionChange={rows => {
                                  if (rows[0] == null || rows[0] == undefined) {
                                    this.props.selectedItem({});
                                    return 0;
                                  }
                                  this.setState({ selectedRows: rows });
                                  this.props.selectedItem(rows[0]);
                                }}
                                actions={[
                                  {
                                    icon: Create,
                                    onClick: (event, rowData) => {
                                      this.props.selectedItem(rowData);
                                      this.props.itemType("ITEM_EDIT");
                                      this.handleDialog(
                                        "BILLOFENTRY_INVOICE_ITEM"
                                      );
                                    }
                                  },
                                  {
                                    icon: Delete,
                                    onClick: (event, rowData) => {
                                      const form2Data = [
                                        ...this.state.form2Data
                                      ];
                                      form2Data.splice(
                                        form2Data.indexOf(rowData),
                                        1
                                      );
                                      this.setState({ form2Data });
                                    }
                                  }
                                ]}
                                components={{
                                  Toolbar: () => {
                                    return (
                                      <Grid
                                        container
                                        direction="row"
                                        justify="flex-end"
                                        alignItems="center"
                                      >
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            this.props.itemType("ITEM_ADD");
                                            this.handleDialog(
                                              "BILLOFENTRY_INVOICE_ITEM"
                                            );
                                          }}
                                        >
                                          + Add Row
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // this.props.itemType("ITEM_EDIT");
                                            // this.handleDialog(
                                            //   "BILLOFENTRY_INVOICE_ITEM"
                                            // );
                                            // let selectedData =
                                            //   state.data[
                                            //     state.data.indexOf(
                                            //       state.selectedRows[0]
                                            //     )
                                            //   ];
                                            // state.selectedData.push(selectedData);
                                            // setItem("ITEM_EDIT");
                                            // handleClickOpen();
                                          }}
                                        >
                                          Open Form
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // let form2Data = this.state.form2Data.filter(
                                            //   e => {
                                            //     return !this.state.selectedRows.some(
                                            //       s => {
                                            //         if (s.id === e.id)
                                            //           return true;
                                            //       }
                                            //     );
                                            //   }
                                            // );
                                            // this.setState({ form2Data });
                                          }}
                                        >
                                          Delete Row
                                        </Button>
                                      </Grid>
                                    );
                                  },
                                  Body: props => {
                                    let amount = 0;
                                    let duty = 0;
                                    props.renderData.forEach(e => {
                                      let camount =
                                        parseInt(e.qty) * parseInt(e.unitPrice);
                                      amount += parseInt(camount);
                                      duty += parseInt(e.duty) || 0;
                                    });
                                    return (
                                      <React.Fragment>
                                        <MTableBody {...props} />
                                        <TableFooter>
                                          <TableRow>
                                            <TableCell
                                              rowSpan={4}
                                              colSpan={5}
                                            />
                                            <TableCell>Amount</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(amount).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Duty</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(duty).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                        </TableFooter>
                                      </React.Fragment>
                                    );
                                  }
                                }}
                              />
                            </Grid>
                          </Grid>
                        </Form>
                      );
                    }}
                  </Formik>
                </TabContainer>
              )}
              {this.state.formTabContainer === 2 && (
                <TabContainer>
                  <Formik
                    initialValues={{
                      transactionNo: importBill.bufferData.header.transactionNo,
                      transactionDate:
                        importBill.bufferData.header.transactionDate,
                      supplier: importBill.bufferData.header.supplier,
                      countryOfOrigin:
                        importBill.bufferData.header.countryOfOrigin,
                      currency: importBill.bufferData.header.currency,
                      rate: importBill.bufferData.header.rate,
                      lCurrency: importBill.bufferData.header.lCurrency,
                      supplierInvoiceNo:
                        importBill.bufferData.header.supplierInvoiceNo,
                      invoiceDate: importBill.bufferData.header.invoiceDate,
                      poReference: importBill.bufferData.header.poReference,
                      creditDays: importBill.bufferData.header.creditDays,
                      creditFrom: importBill.bufferData.header.creditFrom,
                      bl_awbNo: importBill.bufferData.header.bl_awbNo,
                      bl_awbDate: importBill.bufferData.header.bl_awbDate,
                      boeNo: importBill.bufferData.header.boeNo,
                      boeDate: importBill.bufferData.header.boeDate,
                      insurance: importBill.bufferData.header.insurance,
                      freight: importBill.bufferData.header.freight,
                      total: importBill.bufferData.header.total
                    }}
                    //validationSchema={schema}
                    onSubmit={(values, { setSubmitting }) => {
                      if (
                        this.state.submitType == "SUBMITTED" &&
                        importBill.bufferData.formStatus == "OPEN"
                      ) {
                        values.transactionNo = `IMP/001/1906/0000${this.props
                          .importBill.submittedInvoices.length + 1}`;
                        values.formStatus = "SUBMITTED";
                      } else {
                        values.transactionNo =
                          importBill.bufferData.transactionNo;
                        values.formStatus = importBill.bufferData.formStatus;
                      }
                      values.narration = importBill.bufferData.header.narration;
                      let otherCharges = 0;
                      this.state.form3Data.forEach(e => {
                        otherCharges += e.total;
                      });
                      let currentInvoice = {
                        id: importBill.bufferData._id,
                        transactionNo: values.transactionNo,
                        transactionDate: values.transactionDate,
                        formStatus: values.formStatus,
                        supplier: values.supplier,
                        invoiceNo: values.supplierInvoiceNo,
                        invoiceDate: values.invoiceDate,
                        fc: values.currency,
                        fcAmt: importBill.bufferData.fcAmt,
                        duty: importBill.bufferData.duty,
                        other: otherCharges,
                        header: values,
                        supplierInvoice: importBill.bufferData.supplierInvoice,
                        billOfEntry: importBill.bufferData.billOfEntry,
                        otherCharges: this.state.form3Data,
                        journal: importBill.bufferData.journal
                      };
                      this.props.bufferData(currentInvoice);
                      if (!this.state.navigation) {
                        apiCallCreator.updateImportInvoice(currentInvoice);
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                        this.handleDialog("INVOICE_FORM");
                      }
                      if (
                        currentInvoice.id == undefined &&
                        importBill.formType == "FORM_ADD" &&
                        !this.state.navigation
                      ) {
                        apiCallCreator.addImportInvoice(
                          currentInvoice,
                          currentInvoice,
                          this.props.bufferData
                        );
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                      }
                      if (this.state.navigation) {
                        this.handleInnerTabChange(this.state.navigationSide);
                      }
                      this.setState({ navigation: false });
                      setSubmitting(false);
                    }}
                  >
                    {({ values, handleSubmit }) => {
                      this.formHandler = handleSubmit;
                      return (
                        <Form autoComplete="off">
                          <Grid container spacing={0}>
                            {importBill.formType != "FORM_ADD" ? (
                              <Grid item xs={3}>
                                <Field
                                  name="transactionNo"
                                  label="Transaction No"
                                  component={TextField}
                                  value={values.transactionNo}
                                  style={{ width: "95%", marginBottom: "10px" }}
                                  variant="outlined"
                                  margin="normal"
                                  disabled
                                  InputLabelProps={{
                                    shrink: true
                                  }}
                                />
                              </Grid>
                            ) : (
                              ""
                            )}
                            {fieldMapper(form1, values)}
                            <Grid item style={{ width: "100%" }}>
                              <MaterialTable
                                title="Details"
                                columns={columnsForm3}
                                data={this.state.form3Data}
                                options={{
                                  sort: false,
                                  search: false,
                                  paging: false
                                  //selection: true
                                }}
                                onSelectionChange={rows => {
                                  if (rows[0] == null || rows[0] == undefined) {
                                    this.props.selectedItem({});
                                    return 0;
                                  }
                                  this.setState({ selectedRows: rows });
                                  this.props.selectedItem(rows[0]);
                                }}
                                actions={[
                                  {
                                    icon: Create,
                                    onClick: (event, rowData) => {
                                      this.props.selectedItem(rowData);
                                      this.props.itemType("ITEM_EDIT");
                                      this.handleDialog(
                                        "OTHERCHARGES_INVOICE_ITEM"
                                      );
                                    }
                                  },
                                  {
                                    icon: Delete,
                                    onClick: (event, rowData) => {
                                      const form3Data = [
                                        ...this.state.form3Data
                                      ];
                                      form3Data.splice(
                                        form3Data.indexOf(rowData),
                                        1
                                      );
                                      this.setState({ form3Data });
                                    }
                                  }
                                ]}
                                components={{
                                  Toolbar: () => {
                                    return (
                                      <Grid
                                        container
                                        direction="row"
                                        justify="flex-end"
                                        alignItems="center"
                                      >
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            this.props.itemType("ITEM_ADD");
                                            this.handleDialog(
                                              "OTHERCHARGES_INVOICE_ITEM"
                                            );
                                          }}
                                        >
                                          + Add Row
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // this.props.itemType("ITEM_EDIT");
                                            // this.handleDialog(
                                            //   "OTHERCHARGES_INVOICE_ITEM"
                                            // );
                                          }}
                                        >
                                          Open Form
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // let form3Data = this.state.form3Data.filter(
                                            //   e => {
                                            //     return !this.state.selectedRows.some(
                                            //       s => {
                                            //         if (s.id === e.id)
                                            //           return true;
                                            //       }
                                            //     );
                                            //   }
                                            // );
                                            // this.setState({ form3Data });
                                          }}
                                        >
                                          Delete Row
                                        </Button>
                                      </Grid>
                                    );
                                  },
                                  Body: props => {
                                    let totalAmount = 0;
                                    let totalTaxes = 0;
                                    props.renderData.forEach(e => {
                                      totalAmount += parseInt(e.amount);
                                      totalTaxes += parseInt(e.taxes);
                                    });
                                    let total = totalAmount + totalTaxes;
                                    // let subTotal = 0;
                                    // props.renderData.forEach(e => {
                                    //   let total = e.qty * e.unitPrice;
                                    //   subTotal += parseInt(total);
                                    // });
                                    // let total =
                                    //   subTotal + values.insurance + values.freight;
                                    return (
                                      <React.Fragment>
                                        <MTableBody {...props} />
                                        <TableFooter>
                                          <TableRow>
                                            <TableCell
                                              rowSpan={3}
                                              colSpan={5}
                                            />
                                            <TableCell>Amount</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(totalAmount).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Taxes</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(totalTaxes).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Total</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(total).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                        </TableFooter>
                                      </React.Fragment>
                                    );
                                  }
                                }}
                              />
                            </Grid>
                          </Grid>
                        </Form>
                      );
                    }}
                  </Formik>
                </TabContainer>
              )}
              {this.state.formTabContainer === 3 && (
                <TabContainer>
                  <Formik
                    initialValues={{
                      transactionDate:
                        importBill.bufferData.header.transactionDate,
                      narration:
                        importBill.formType == "FORM_ADD"
                          ? ""
                          : importBill.bufferData.header.narration
                    }}
                    //validationSchema={schema}
                    onSubmit={(values, { setSubmitting }) => {
                      console.log(this.state.submitType);
                      console.log(importBill.bufferData.formStatus);
                      let temp = importBill.bufferData.header;
                      temp.narration = values.narration;
                      if (
                        this.state.submitType == "SUBMITTED" &&
                        importBill.bufferData.formStatus == "OPEN"
                      ) {
                        temp.transactionNo = `IMP/001/1906/0000${this.props
                          .importBill.submittedInvoices.length + 1}`;
                        temp.formStatus = "SUBMITTED";
                      } else {
                        temp.transactionNo =
                          importBill.bufferData.transactionNo;
                        temp.formStatus = importBill.bufferData.formStatus;
                      }

                      let currentInvoice = {
                        id: importBill.bufferData._id,
                        transactionNo: temp.transactionNo,
                        transactionDate: importBill.bufferData.transactionDate,
                        formStatus: temp.formStatus,
                        supplier: importBill.bufferData.supplier,
                        invoiceNo: importBill.bufferData.supplierInvoiceNo,
                        invoiceDate: importBill.bufferData.invoiceDate,
                        fc: importBill.bufferData.currency,
                        fcAmt: importBill.bufferData.fcAmt,
                        duty: importBill.bufferData.duty,
                        other: importBill.bufferData.other,
                        header: temp,
                        supplierInvoice: importBill.bufferData.supplierInvoice,
                        billOfEntry: importBill.bufferData.billOfEntry,
                        otherCharges: importBill.bufferData.otherCharges,
                        journal: this.state.form4Data
                      };
                      this.props.bufferData(currentInvoice);
                      if (
                        currentInvoice.id == undefined &&
                        importBill.formType == "FORM_ADD"
                      ) {
                        apiCallCreator.addImportInvoice(
                          currentInvoice,
                          currentInvoice,
                          this.props.bufferData
                        );
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                      }
                      if (!this.state.navigation) {
                        apiCallCreator.updateImportInvoice(currentInvoice);
                        this.props.enqueueSnackbar(
                          `${currentInvoice.transactionNo} ${
                            importBill.formType == "FORM_ADD"
                              ? "Added"
                              : "Editted"
                          } Successfully`,
                          { variant: "success" }
                        );
                        this.handleDialog("INVOICE_FORM");
                      }
                      if (this.state.navigation) {
                        this.handleInnerTabChange(this.state.navigationSide);
                      }
                      this.setState({ navigation: false });
                      setSubmitting(false);
                    }}
                  >
                    {({ values, handleSubmit }) => {
                      this.formHandler = handleSubmit;
                      return (
                        <Form autoComplete="off">
                          <Grid container spacing={0}>
                            {fieldMapper(form4, values)}
                            <Grid item style={{ width: "100%" }}>
                              <MaterialTable
                                title="Details"
                                columns={columnsForm4}
                                data={this.state.form4Data}
                                options={{
                                  sort: false,
                                  search: false,
                                  paging: false
                                  //selection: true
                                }}
                                onSelectionChange={rows => {
                                  if (rows[0] == null || rows[0] == undefined) {
                                    this.props.selectedItem({});
                                    return 0;
                                  }
                                  this.setState({ selectedRows: rows });
                                  this.props.selectedItem(rows[0]);
                                }}
                                actions={[
                                  {
                                    icon: Create,
                                    onClick: (event, rowData) => {
                                      this.props.selectedItem(rowData);
                                      this.props.itemType("ITEM_EDIT");
                                      this.handleDialog("JOURNAL_INVOICE_ITEM");
                                    }
                                  },
                                  {
                                    icon: Delete,
                                    onClick: (event, rowData) => {
                                      const form4Data = [
                                        ...this.state.form4Data
                                      ];
                                      form4Data.splice(
                                        form4Data.indexOf(rowData),
                                        1
                                      );
                                      this.setState({ form4Data });
                                    }
                                  }
                                ]}
                                components={{
                                  Toolbar: () => {
                                    return (
                                      <Grid
                                        container
                                        direction="row"
                                        justify="flex-end"
                                        alignItems="center"
                                      >
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            this.props.itemType("ITEM_ADD");
                                            this.handleDialog(
                                              "JOURNAL_INVOICE_ITEM"
                                            );
                                          }}
                                        >
                                          + Add Row
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // this.props.itemType("ITEM_EDIT");
                                            // this.handleDialog(
                                            //   "JOURNAL_INVOICE_ITEM"
                                            // );
                                          }}
                                        >
                                          Open Form
                                        </Button>
                                        <Button
                                          color="primary"
                                          size="small"
                                          onClick={() => {
                                            // let form1Data = this.state.form1Data.filter(
                                            //   e => {
                                            //     return !this.state.selectedRows.some(
                                            //       s => {
                                            //         if (s.id === e.id)
                                            //           return true;
                                            //       }
                                            //     );
                                            //   }
                                            // );
                                            // this.setState({ form1Data });
                                          }}
                                        >
                                          Delete Row
                                        </Button>
                                      </Grid>
                                    );
                                  },
                                  Body: props => {
                                    let totalCredit = 0;
                                    let totalDebit = 0;
                                    props.renderData.forEach(e => {
                                      totalCredit += parseInt(e.credit);
                                      totalDebit += parseInt(e.debit);
                                    });
                                    return (
                                      <React.Fragment>
                                        <MTableBody {...props} />
                                        <TableFooter>
                                          <TableRow>
                                            <TableCell
                                              rowSpan={2}
                                              colSpan={2}
                                            />
                                            <TableCell>Credit</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(totalCredit).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                          <TableRow>
                                            <TableCell>Debit</TableCell>
                                            <TableCell
                                              colSpan={2}
                                              align="right"
                                            >
                                              {parseInt(totalDebit).toFixed(2)}
                                            </TableCell>
                                          </TableRow>
                                        </TableFooter>
                                      </React.Fragment>
                                    );
                                  }
                                }}
                              />
                            </Grid>
                          </Grid>
                        </Form>
                      );
                    }}
                  </Formik>
                </TabContainer>
              )}
            </Paper>
          </DialogContent>
        </Dialog>
        {/************************* FORM INVOICE DIALOG ENDS *************************/}
      </React.Fragment>
    );
  }
}

function fieldMapper(fieldData, values) {
  return fieldData.map((e, i) => {
    switch (e.type) {
      case "autoComplete":
        return (
          <Grid key={i} item xs={e.size}>
            <div style={{ width: "95%", marginTop: "16px" }}>
              <Field
                name={e.name}
                render={props => {
                  return (
                    <NewAutoComplete
                      key={i + e.name}
                      name={e.name}
                      formik={props}
                      data={e.suggestions}
                      label={e.label}
                      value={values[e.name]}
                    />
                  );
                }}
              />
            </div>
          </Grid>
        );
      case "select":
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              select
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={TextField}
              margin="normal"
              InputLabelProps={{
                shrink: true
              }}
            >
              <MenuItem value="usd">$</MenuItem>
              <MenuItem value="eur">€</MenuItem>
              <MenuItem value="btc">฿</MenuItem>
              <MenuItem value="jpy">¥</MenuItem>
            </Field>
          </Grid>
        );
      case "date":
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              inputVariant="outlined"
              render={props => {
                return (
                  <DatePickerMM
                    initialValue={values[e.name] == "" ? null : values[e.name]}
                    name={e.name}
                    label={e.label}
                    formik={props.form}
                  />
                );
              }}
            />
          </Grid>
        );
      default:
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              type={e.type}
              value={values[e.name]}
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={TextField}
              margin="normal"
              multiline={e.multiline}
              InputLabelProps={{
                shrink: true
              }}
            />
          </Grid>
        );
    }
  });
}

function dateFormatter(data) {
  let d = new Date(data);
  return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
}

function FielddateFormatter(data) {
  variant = "outlined";
  let d = new Date(data);
  let day = d.getDate() < 10 ? `${"0" + (d.getDate() + 1)}` : d.getDate() + 1;
  let month =
    d.getMonth() < 10 ? `${"0" + (d.getMonth() + 1)}` : d.getMonth() + 1;
  return `${d.getFullYear()}-${month}-${day}`;
}

const form1 = [
  {
    name: "transactionDate",
    label: "Transaction Date*",
    size: 3,
    type: "date"
  },
  {
    name: "supplier",
    label: "Supplier*",
    size: 3,
    type: "autoComplete",
    suggestions: [...SupplierJson]
  },
  {
    name: "countryOfOrigin",
    label: "Country Of Origin*",
    size: 3,
    type: "autoComplete",
    suggestions: [...CountryJson]
  },
  {
    name: "currency",
    label: "Curr*",
    size: 1,
    type: "autoComplete",
    suggestions: CurrencyJson
  },
  { name: "rate", label: "Rate*", size: 1, type: "number" },
  {
    name: "lCurrency",
    label: "Curr*",
    size: 1,
    type: "autoComplete",
    suggestions: CurrencyJson
  },
  {
    name: "supplierInvoiceNo",
    label: "Supplier Invoice No*",
    size: 3
  },
  { name: "invoiceDate", label: "Invoice Date*", size: 3, type: "date" },
  { name: "poReference", label: "P.O. Reference*", size: 3 },
  { name: "creditDays", label: "Credit Days*", size: 2, type: "number" },
  {
    name: "creditFrom",
    label: "Crd Frm*",
    size: 1,
    type: "autoComplete",
    suggestions: CreditJson
  },
  { name: "bl_awbNo", label: "B/L or AWB No", size: 3, type: "string" },
  { name: "bl_awbDate", label: "B/L or AWB Date", size: 3, type: "date" },
  { name: "boeNo", label: "BOE No", size: 3, type: "text" },
  { name: "boeDate", label: "BOE Date", size: 3, type: "date" },
  { name: "insurance", label: "insurance", size: 3, type: "number" },
  { name: "freight", label: "freight", size: 3, type: "number" },
  { name: "total", label: "Total", size: 3, type: "number" }
];

const form4 = [
  {
    name: "transactionDate",
    label: "Transaction Date",
    size: 4,
    type: "date",
    multiline: false
  },
  {
    name: "narration",
    label: "Narration",
    size: 8,
    type: "textarea",
    multiline: true
  }
];

const schema = Yup.object().shape({
  transactionDate: Yup.string().required("Required"),
  supplier: Yup.string().required("Required"),
  countryOfOrigin: Yup.string().required("Required"),
  currency: Yup.string().required("Required"),
  rate: Yup.number().required("Required"),
  supplierInvoiceNo: Yup.string().required("Required"),
  lCurrency: Yup.string().required("Required"),
  // invoiceDate: Yup.string().test("sd", "dsds", val => {
  //   return val == Yup.ref("transactionDate") ? true : false;
  // }),
  invoiceDate: Yup.string().required("Required"),
  poReference: Yup.string().required("Required"),
  creditDays: Yup.number()
    .required("Required")
    .integer("Provide Integer"),
  creditFrom: Yup.string().required("Required"),
  bl_awbNo: Yup.string(),
  bl_awbDate: Yup.string()
    .notRequired()
    .when("bl_awbNo", {
      is: val => val !== undefined,
      then: Yup.string().required("Required"),
      otherwise: Yup.string().notRequired()
    }),
  boeNo: Yup.string(),
  boeDate: Yup.string()
    .notRequired()
    .when("boeNo", {
      is: val => val !== undefined,
      then: Yup.string().required("Required"),
      otherwise: Yup.string().notRequired()
    })
});

const journalSchema = Yup.object().shape({
  accountHead: Yup.string().required("Required"),
  credit: Yup.number().required("Required"),
  debit: Yup.number().required("Required")
});

const otherSchema = Yup.object().shape({
  party: Yup.string().required("Required"),
  invoiceNo: Yup.string().required("Required"),
  date: Yup.string().required("Required"),
  item: Yup.string().required("Required"),
  amount: Yup.number().required("Required"),
  taxes: Yup.number().required("Required")
});

const boiSchema = Yup.object().shape({
  item: Yup.string().required("Required"),
  qty: Yup.number().required("Required"),
  uom: Yup.string().required("Required"),
  unitPrice: Yup.number().required("Required"),
  duty: Yup.number().required("Required")
});

const supplierSchema = Yup.object().shape({
  item: Yup.string().required("Required"),
  qty: Yup.number().required("Required"),
  uom: Yup.string().required("Required"),
  packSize: Yup.number().required("Required"),
  packUom: Yup.string().required("Required"),
  unitPrice: Yup.number().required("Required")
});

const boeSchema = Yup.object().shape({
  transactionDate: Yup.string().required("Required"),
  supplier: Yup.string().required("Required"),
  countryOfOrigin: Yup.string().required("Required"),
  currency: Yup.string().required("Required"),
  rate: Yup.number().required("Required"),
  supplierInvoiceNo: Yup.string().required("Required"),
  lCurrency: Yup.string().required("Required"),
  invoiceDate: Yup.string().required("Required"),
  poReference: Yup.string().required("Required"),
  creditDays: Yup.string().required("Required"),
  creditFrom: Yup.string().required("Required"),
  boeNo: Yup.string().required("Required"),
  bl_awbNo: Yup.string(),
  bl_awbDate: Yup.string()
    .notRequired()
    .when("bl_awbNo", {
      is: val => val !== undefined,
      then: Yup.string().required("Required"),
      otherwise: Yup.string().notRequired()
    }),
  boeNo: Yup.string().required("required"),
  boeDate: Yup.string().required("required")
});

// bl_awbNo: Yup.string()
//     .oneOf([Yup.ref("bl_awbDate"), null], "BL/Awb Date Required")
//     .required("Required"),
//   bl_awbDate: Yup.string()
//     .oneOf([Yup.ref("bl_awbNo"), null], "BL/Awb No Required")
//     .required("Required"),

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return {
    formType: val => dispatch(actionCreator.ChangeFormType(val)),
    itemType: val => dispatch(actionCreator.ChangeItemType(val)),
    bufferData: val => dispatch(actionCreator.BufferData(val)),
    selectedItem: val => dispatch(actionCreator.SelectedItem(val)),
    getAllInvoice: val => dispatch(actionCreator.GetAllInvoice(val)),
    formSubmitType: val => dispatch(actionCreator.FormSubmitType(val))
  };
};

const WithStylesImportBill = withStyles(styles)(ImportBill);
const WithImportBill = withSnackbar(WithStylesImportBill);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WithImportBill);

const columnsSearch = [
  {
    title: "Trans #",
    field: "transactionNo"
  },
  {
    title: "Trans Dt",
    field: "transactionDate",
    render: rowData => {
      if (rowData.transactionDate == "") {
        rowData.transactionDate = Date.now();
      }
      let date = new Date(rowData.transactionDate);
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return <Typography>{month + "/" + day + "/" + year}</Typography>;
    }
  },
  { title: "Supplier", field: "supplier" },
  { title: "Inv #", field: "invoiceNo" },
  {
    title: "Inv Dt",
    field: "invoiceDate",
    render: rowData => {
      if (rowData.invoiceDate == "") {
        rowData.invoiceDate = Date.now();
      }
      let date = new Date(rowData.invoiceDate);
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return <Typography>{month + "/" + day + "/" + year}</Typography>;
    }
  },
  {
    title: "FC",
    field: "fc"
  },
  {
    title: "FC Amt",
    field: "fcAmt",
    type: "numeric",
    cellStyle: {
      width: 10,
      maxWidth: 10
    },
    render: rowData => {
      return <Typography>{parseInt(rowData.fcAmt).toFixed(2)}</Typography>;
    }
  },
  {
    title: "Duty",
    field: "duty",
    type: "numeric",
    render: rowData => {
      return <Typography>{parseInt(rowData.duty).toFixed(2)}</Typography>;
    }
  },
  {
    title: "Other",
    field: "other",
    type: "numeric",
    render: rowData => {
      return <Typography>{parseInt(rowData.other).toFixed(2)}</Typography>;
    }
  },
  {
    title: "Total Price",
    field: "total",
    render: rowData => {
      let total =
        parseInt(rowData.fcAmt || 0) +
        parseInt(rowData.duty || 0) +
        parseInt(rowData.other || 0);

      return <Typography>{parseInt(total).toFixed(2)}</Typography>;
    }
  }
];

const columnsForm1 = [
  { title: "Item", field: "item" },
  { title: "Qty", field: "qty", type: "numeric" },
  {
    title: "UOM",
    field: "uom"
  },
  { title: "Pack Size", field: "packSize", type: "numeric" },
  {
    title: "UOM",
    field: "packUom"
  },
  {
    title: "Curr",
    field: "currency"
  },
  {
    title: "Unit Price",
    field: "unitPrice",
    type: "numeric",
    render: rowData => {
      return <Typography>{rowData.unitPrice.toFixed(2) || 0}</Typography>;
    }
  },
  {
    title: "Total",
    field: "total",
    render: rowData => {
      let total = rowData.qty * rowData.unitPrice;
      rowData.total = total;
      return <Typography>{rowData.total.toFixed(2) || 0}</Typography>;
    }
  }
];

const columnsForm2 = [
  { title: "Item", field: "item" },
  {
    title: "Qty",
    field: "qty",
    type: "numeric",
    render: rowData => {
      return <Typography>{parseInt(rowData.qty).toFixed(2) || 0}</Typography>;
    }
  },
  {
    title: "UOM",
    field: "uom"
  },
  {
    title: "Unit Price",
    field: "unitPrice",
    type: "numeric",
    render: rowData => {
      return <Typography>{rowData.unitPrice.toFixed(2) || 0}</Typography>;
    }
  },
  {
    title: "Amount",
    field: "amount",
    type: "numeric",
    render: rowData => {
      rowData.amount = parseInt(rowData.qty) * parseInt(rowData.unitPrice);
      return <Typography>{rowData.amount.toFixed(2) || 0}</Typography>;
    }
  },
  {
    title: "Duty",
    field: "duty",
    type: "numeric",
    render: rowData => {
      return <Typography>{parseInt(rowData.duty).toFixed(2) || 0}</Typography>;
    }
  }
];

const columnsForm3 = [
  { title: "Party", field: "party" },
  { title: "Inv #", field: "invoiceNo" },
  {
    title: "Date",
    field: "date",
    render: rowData => {
      let date = new Date(rowData.date);
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      return <Typography>{month + "/" + day + "/" + year}</Typography>;
    }
  },
  { title: "Item", field: "item" },
  {
    title: "Amount",
    field: "amount",
    type: "numeric",
    render: rowData => {
      return (
        <Typography>{parseInt(rowData.amount).toFixed(2) || 0}</Typography>
      );
    }
  },
  {
    title: "Taxes",
    field: "taxes",
    type: "numeric",
    render: rowData => {
      return <Typography>{parseInt(rowData.taxes).toFixed(2) || 0}</Typography>;
    }
  },
  {
    title: "Total",
    field: "total",
    render: rowData => {
      rowData.total = parseInt(rowData.amount) + parseInt(rowData.taxes);
      return <Typography>{rowData.total || 0}</Typography>;
    }
  }
];

const columnsForm4 = [
  { title: "Account Head", field: "accountHead" },
  {
    title: "Credit",
    field: "credit",
    type: "numeric",
    render: rowData => {
      return (
        <Typography>{parseInt(rowData.credit).toFixed(2) || 0}</Typography>
      );
    }
  },
  {
    title: "Debit",
    field: "debit",
    type: "numeric",
    render: rowData => {
      return (
        <Typography>{parseInt(rowData.credit).toFixed(2) || 0}</Typography>
      );
    }
  }
];
