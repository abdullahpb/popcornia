import React, { Component } from "react";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Divider,
  Grid,
  Paper,
  MenuItem,
  Radio,
  TableCell,
  TableRow,
  TableFooter,
  RadioGroup,
  FormControlLabel
} from "@material-ui/core";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { Formik, Form } from "formik";
const Field = dynamic(() => import("../../components/formik"), {
  ssr: false
});
import { TextField } from "formik-material-ui";
import * as Yup from "yup";

import IconButton from "@material-ui/core/IconButton";
import { Clear, ArrowForward, ArrowBack } from "@material-ui/icons";

import Chip from "@material-ui/core/Chip";

import MaterialTable, { MTableBody } from "material-table";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogFooter from "@material-ui/core/DialogTitle";

import AutoComplete from "../../components/autocomplete.js";
import NewAutoComplete from "../../components/newAutoComplete";
import SupplierJson from "../../assets/json/supplierJson.json";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "10px",
    margin: "10px",
    width: "50%"
  },
  divider: {
    color: "#e0e0e0",
    borderStyle: "solid",
    borderColor: "#e0e0e0",
    borderWidth: "2px"
  },
  dialogPaper: {
    minHeight: "50vh",
    maxHeight: "50vh"
  }
}));

function search() {
  const classes = useStyles();
  console.log(classes);

  const [state, setState] = React.useState({
    columns: [
      { title: "Product Id", field: "productId" },
      { title: "Hybrid", field: "hybrid" },
      {
        title: "Status",
        field: "processStatus",
        render: rowData => (
          <Chip
            label={rowData.processStatus}
            style={{ width: "150px" }}
            color="primary"
          />
        )
      },
      {
        title: "",
        field: "",
        render: rowData => (
          <Button
            variant="outlined"
            color="primary"
            size="small"
            style={{
              width: "200px",
              marginBottom: "5px",
              borderRadius: "25px"
            }}
            onClick={() => {
              setState({ ...state, dialogStatus: true });
            }}
          >
            Update Info
          </Button>
        )
      }
    ],
    DispatchFields: [
      { name: "hybrid", label: "Hybrid", size: 3 },
      {
        name: "placeOfOrigin",
        label: "Place Of Origin",
        size: 3
      },
      { name: "dispatchDocNo", label: "Dispatch Doc No", size: 3 },
      { name: "purchaseSlipNo", label: "Purchase Slip No", size: 3 },
      { name: "truckNumber", label: "Truck Number", size: 3 },
      { name: "noOfBags", label: "No Of Bags", size: 3, type: "number" },
      {
        name: "dispatchedWeight",
        label: "Dispatched Weight",
        size: 3,
        type: "number"
      },
      {
        name: "dispatchDateAndTime",
        label: "Dispatch Date And Time",
        size: 3,
        type: "dateAndTime"
      }
    ],
    ArrivalFields: [
      {
        name: "arrivalDateAndTime",
        label: "Arrival Date And Time",
        size: 3,
        type: ""
      },
      { name: "travelTime", label: "Travel Time (Hrs)", size: 3, type: "" },
      { name: "noOfBags", label: "No Of Bags", size: 3, type: "" },
      {
        name: "bagsShortOrExcess",
        label: "Bags Short/Excess",
        size: 3,
        type: ""
      },
      { name: "arrivalWeight", label: "Arrival Weight", size: 3, type: "" },
      { name: "weightScaleDoc", label: "Weight Scale Doc", size: 3, type: "" },
      { name: "diffInWeight", label: "Diff In Weight", size: 3, type: "" },
      {
        name: "arrivalMoisture",
        label: "Arrival Moisture %",
        size: 3,
        type: ""
      },
      { name: "binNo", label: "Bin No", size: 3, type: "" },
      {
        name: "intakeDateAndTime",
        label: "Intake Date And Time",
        size: 3,
        type: ""
      },
      { name: "sorting", label: "Sorting", size: 2, type: "" },
      { name: "sortingWastage", label: "Sorting Wastage", size: 2, type: "" },
      { name: "waitingTime", label: "Waiting Time", size: 2, type: "" }
    ],
    DryingFields: [
      { name: "binNo", label: "Bin No", size: 3, type: "" },
      { name: "hybrid", label: "Hybrid", size: 3, type: "" },
      { name: "dispatchDocNo", label: "Dispatch Doc No", size: 3, type: "" },
      {
        name: "dispatchWetCobWeight",
        label: "Dispatch Wet Cob Weight",
        size: 3,
        type: ""
      },
      {
        name: "dispatchStartDateAndTime",
        label: "Dispatch Start Date And Time",
        size: 3,
        type: ""
      },
      { name: "initialMoisture", label: "Initial Moisture", size: 3, type: "" },
      { name: "finalMoisture", label: "Final Moisture", size: 3, type: "" },
      {
        name: "totalDryingTime",
        label: "Total Drying Time",
        size: 3,
        type: ""
      },
      {
        name: "reductionInMoisture",
        label: "Reduction In Moisture",
        size: 3,
        type: ""
      },
      { name: "dryingRate", label: "dryingRate", size: 3, type: "" }
    ],
    ShellingFields: [
      {
        name: "shellingStartDateAndTime",
        label: "Shelling Start Date And Time",
        size: 3,
        type: ""
      },
      {
        name: "shellingEndDateAndTime",
        label: "Shelling End Date And Time",
        size: 3,
        type: ""
      },
      {
        name: "totalShellingHours",
        label: "Total Shelling Hours",
        size: 3,
        type: ""
      },
      { name: "totalRawSeed", label: "Total Raw Seed", size: 3, type: "" },
      { name: "moisture", label: "Moisture %", size: 3, type: "" },
      { name: "noOfBags", label: "No Of Bags", size: 3, type: "" },
      {
        name: "rawSeedRecovery",
        label: "Raw Seed Recovery",
        size: 3,
        type: ""
      },
      {
        name: "rawKernelsLotNumber",
        label: "Raw Kernels Lot Number",
        size: 3,
        type: ""
      }
    ],
    StorageFields: [
      { name: "hybrid", label: "Hybrid", size: 3, type: "" },
      { name: "binNumber", label: "Bin Number", size: 3, type: "" },
      { name: "lotNumber", label: "Lot Number", size: 3, type: "" },
      {
        name: "dispatchedMoisture",
        label: "Dispatched Moisture",
        size: 3,
        type: ""
      },
      {
        name: "dispatchedDocNo",
        label: "Dispatched Doc No",
        size: 3,
        type: ""
      },
      { name: "noOfBags", label: "No Of Bags", size: 3, type: "" },
      {
        name: "dispatchedWeight",
        label: "Dispatched Weight",
        size: 3,
        type: ""
      },
      {
        name: "weighbridgeSlipNo",
        label: "Weighbridge Slip No",
        size: 3,
        type: ""
      },
      {
        name: "dispatchDateAndTime",
        label: "Dispatch Date And Time",
        size: 3,
        type: ""
      },
      {
        name: "transporterDetails",
        label: "Transporter Details",
        size: 3,
        type: ""
      },
      { name: "truckNumber", label: "Truck Number", size: 3, type: "" },
      { name: "destination", label: "Destination", size: 3, type: "" }
    ],
    data: [],
    dialogStatus: false,
    dialogTabStatus: 0
  });

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          abd: "",
          cc: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          setSubmitting(false);
        }}
        validationSchema={schema}
      >
        {({ values }) => (
          <Form autoComplete="off">
            <Field
              name="abd"
              render={props => {
                return (
                  <NewAutoComplete
                    name="abd"
                    key={1}
                    formik={props}
                    data={SupplierJson}
                    label="Test"
                    value={values.abd}
                  />
                );
              }}
            />
            <Button
              color="primary"
              variant="outlined"
              type="submit"
              size="small"
              style={{
                width: "200px",
                borderRadius: "25px"
              }}
              onClick={() => {
                console.log("err");
              }}
            >
              Submit
            </Button>
          </Form>
        )}
      </Formik>
      <Grid container>
        <Grid item xs={12}>
          <MaterialTable
            columns={state.columns}
            data={columnData}
            options={{
              sort: false,
              search: false,
              paging: false
            }}
            components={{
              Toolbar: () => {
                return (
                  <div
                    style={{
                      backgroundColor: "#1a237e",
                      height: "5px",
                      borderTopLeftRadius: 25,
                      borderTopRightRadius: 25
                    }}
                  />
                );
              }
            }}
          />
        </Grid>
      </Grid>
      {/************************* UPDATE DIALOG STARTS *************************/}
      <Dialog
        open={state.dialogStatus}
        fullWidth={true}
        classes={{ paper: classes.dialogPaper }}
        maxWidth={"xl"}
        onClose={() => setState({ ...state, dialogStatus: false })}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title" style={{ margin: 0, padding: 0 }}>
          <Paper square={true} style={{ flexGrow: 1, margin: 0, padding: 0 }}>
            <Grid container alignItems="center">
              <Grid item xs={10}>
                <Tabs
                  value={state.dialogTabStatus}
                  onChange={(evt, val) => {
                    setState({ ...state, dialogTabStatus: val });
                    console.log(val);
                  }}
                >
                  <Tab label="Dispatch" />
                  <Tab label="Arrival" />
                  <Tab label="Drying" />
                  <Tab label="Shelling" />
                  <Tab label="Storage" />
                </Tabs>
              </Grid>
              <Grid item xs={2}>
                <IconButton
                  style={{ float: "right" }}
                  aria-label="Backward"
                  onClick={() => setState({ ...state, dialogStatus: false })}
                >
                  <ArrowBack fontSize="normal" />
                </IconButton>
                <IconButton
                  style={{ float: "right" }}
                  aria-label="Forward"
                  onClick={() => setState({ ...state, dialogStatus: false })}
                >
                  <ArrowForward fontSize="normal" />
                </IconButton>
                <IconButton
                  style={{ float: "right" }}
                  aria-label="Cancle"
                  onClick={() => setState({ ...state, dialogStatus: false })}
                >
                  <Clear fontSize="normal" />
                </IconButton>
              </Grid>
            </Grid>
          </Paper>
        </DialogTitle>
        <DialogContent>
          {state.dialogTabStatus === 0 && (
            <div style={{ margin: 10 }}>
              <Formik
                initialValues={{}}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container spacing={0} style={{ padding: 5 }}>
                      {fieldMapper(state.DispatchFields)}
                    </Grid>
                    <Grid
                      container
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          size="small"
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Cancle
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="outlined"
                          size="small"
                          style={{
                            width: "200px",
                            borderRadius: "25px"
                          }}
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          )}
          {state.dialogTabStatus === 1 && (
            <div style={{ margin: 10 }}>
              <Formik
                initialValues={{}}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container spacing={0} style={{ padding: 5 }}>
                      {fieldMapper(state.ArrivalFields)}
                    </Grid>
                    <Grid
                      container
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          size="small"
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Cancle
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="outlined"
                          size="small"
                          style={{
                            width: "200px",
                            borderRadius: "25px"
                          }}
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          )}
          {state.dialogTabStatus === 2 && (
            <div style={{ margin: 10 }}>
              <Formik
                initialValues={{}}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container spacing={0} style={{ padding: 5 }}>
                      {fieldMapper(state.DryingFields)}
                    </Grid>
                    <Grid
                      container
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          size="small"
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Cancle
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="outlined"
                          size="small"
                          style={{
                            width: "200px",
                            borderRadius: "25px"
                          }}
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          )}
          {state.dialogTabStatus === 3 && (
            <div style={{ margin: 10 }}>
              <Formik
                initialValues={{}}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container spacing={0} style={{ padding: 5 }}>
                      {fieldMapper(state.ShellingFields)}
                    </Grid>
                    <Grid
                      container
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                    >
                      <Grid item>
                        <Button
                          color="primary"
                          size="small"
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Cancle
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="outlined"
                          size="small"
                          style={{
                            width: "200px",
                            borderRadius: "25px"
                          }}
                          onClick={() => {
                            console.log("err");
                          }}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          )}
          {state.dialogTabStatus === 4 && (
            <div style={{ margin: 10 }}>
              <Formik
                initialValues={{}}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  setSubmitting(false);
                }}
              >
                {({ values }) => (
                  <Form autoComplete="off">
                    <Grid container spacing={0} style={{ padding: 5 }}>
                      {fieldMapper(state.StorageFields)}
                      <Grid
                        container
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                      >
                        <Grid item>
                          <Button
                            color="primary"
                            size="small"
                            onClick={() => {
                              console.log("err");
                            }}
                          >
                            Cancle
                          </Button>
                        </Grid>
                        <Grid item>
                          <Button
                            color="primary"
                            variant="outlined"
                            size="small"
                            style={{
                              width: "200px",
                              borderRadius: "25px"
                            }}
                            onClick={() => {
                              console.log("err");
                            }}
                          >
                            Submit
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Form>
                )}
              </Formik>
            </div>
          )}
        </DialogContent>
      </Dialog>
      {/************************* UPDATE DIALOG ENDS *************************/}
    </React.Fragment>
  );
}

const schema = Yup.object().shape({
  abd: Yup.string().required("Required"),
  cc: Yup.string().required("Required")
});

export default search;

function fieldMapper(fieldData, values) {
  return fieldData.map((e, i) => {
    switch (e.type) {
      case "autoComplete":
        console.log(e.suggestions);
        return (
          <Grid key={i} item xs={e.size}>
            <div style={{ width: "95%", marginTop: "16px" }}>
              <AutoComplete suggestions={[...e.suggestions]} label={e.label} />
            </div>
          </Grid>
        );
      case "select":
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              select
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={TextField}
              margin="normal"
              InputLabelProps={{
                shrink: true
              }}
            >
              <MenuItem value="usd">$</MenuItem>
              <MenuItem value="euro">$$</MenuItem>
            </Field>
          </Grid>
        );
      default:
        return (
          <Grid key={i} item xs={e.size}>
            <Field
              name={e.name}
              label={e.label}
              type={e.type}
              //value={values[e.name]}
              style={{ width: "95%", marginBottom: "10px" }}
              variant="outlined"
              component={TextField}
              margin="normal"
              multiline={e.multiline}
              InputLabelProps={{
                shrink: true
              }}
            />
          </Grid>
        );
    }
  });
}

const columnData = [
  { productId: "PID/G3/0619/0082", hybrid: "G3", processStatus: "ARRIVAL" },
  { productId: "PID/G3/0619/0070", hybrid: "G1", processStatus: "DISPATCH" },
  { productId: "PID/G3/0619/0062", hybrid: "G2", processStatus: "SHELLING" },
  { productId: "PID/G3/0619/0050", hybrid: "G3", processStatus: "DISPATCH" },
  { productId: "PID/G3/0619/0039", hybrid: "G2", processStatus: "ARRIVAL" }
];
